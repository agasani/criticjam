#CriticJam ReadMe#

This is the front-end for CriticJam

#New DynamoDB Schema#

##Tables##

###item###

- category(hash-key)
- score(range-key)

    * Global Indexes
        - isArticle-time-index

    * Local Indexes
        - category-name-index
        - category-trend-index
        - category-brand-index
        - category-zip-index
        - category-isArticle

###feature-item###

- itemName(hash-key)
- score(range-key)

    * Global Indexes
        - stem-category-index

    * Local Indexes
        - itemName-stem-index

###location###

- state(hash-key)
- zip(range-key)

    * Global Indexes
        - zip-index
        - city-index
        - aka-index

###Getting BeanStalk, BS, deployment working
=======================================
For some reasons Pattern doesn't get installed in the virtualEnv created by BS and installing it globally doesn't solve
the issue. pip install Pattern doesn't work either. To get it to work one needs to run the following cmd:

sudo chown -R ec2-user:ec2-user /opt/python/run/venv/

Then install after getting into the virtual environment with cmd:

source /opt/python/run/venv/bin/activate

Then after that install pattern with cmd:

pip install Pattern==2.6

Also nltk doesn't install successfully due to the fact that setuptools on the ec2 instance is higher than 9.1 by default,
to solve this just install version 9.1 with following command, and install nltk after that.

pip install setuptools==9.1

Then go into the virtual Environment with cmd:
source /opt/python/run/venv/bin/activate
install nltk:

pip install nltk==2.0.4

SSH cmd:

ssh -i /Users/agasani/.ssh/criticjam ec2-user@52.88.69.111

###Getting the Server to Compress Files
========================================
Make an enable_mod_deflate.conf file on the server
with content from:
https://www.clayharmon.com/words/posts/enabling-gzip-compression-on-ec2 and/or
http://www.tonmoygoswami.com/2013/05/how-to-enable-gzip-on-amazon-elastic.html
and place it in /etc/httpd/conf.d folder
then restart server with this CMD:
    sudo /etc/init.d/httpd restart

###Getting the Server to set Content Cache Expiration
=====================================================
Make an expires.conf file on the server
with content from:
https://css-tricks.com/snippets/htaccess/set-expires/
and place the file in /etc/httpdconf.d folder
then restart the server with CMD:
    sudo /etc/init.d/httpd restart

###Miscellaneous
=================
CMD to restart Apache server:
    sudo /etc/init.d/httpd restart
CMD to force overwrite a file in vim:
    :w !sudo tee %

###HTML Compression
To compress HTML use:
https://htmlcompressor.com/compressor/
To compress CSS use:
http://cssminifier.com/

