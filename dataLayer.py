import re
#import pymongo
#from pymongo import MongoClient
import boto.dynamodb2
from boto.dynamodb2.table import Table
from boto.dynamodb2.results import ResultSet, BatchGetResultSet
import json
from decimal import Decimal
from datetime import date
#from httplib2 import Http
#from urllib import urlencode, quote

class DataLayer(object):

    def __init__(self):
        '''
        Some about DynamoDB(DDB):
        It has many ankward things compared to mongodb.
        Key-Value implies that you have to have extra tables and when querying you have to glue up a lot of pieces to come up with your final result.
        So far we have an 'item', 'feature_item', 'category' tables, item contains entities as phones, laptops, you can use boto to see the schemas.. and feature_item has features and jsonified
        pos and neg.item has a list of feature that can be used to retrieve its features and feature_item has item_id that can be used to retrieve feature's item.
        Dynamodb tables have required hash key, and an optional range key which is like secondary key, you can also add indexes..
        Here are some of the DDB's operations: query, scan, get_item, batch_get_item etc.. check out boto docs: http://boto.readthedocs.org/en/latest/dynamodb2_tut.html
        To query you need at least the hash key, and scan is to be avoided coz it's expensive/slow. Use secondary indexes to kinda sort data.
        Query examples:
        dyno.query('feature_item', index_name='item_category-index', limit=2, select='ALL_ATTRIBUTES', key_conditions= {'item_category': { 'AttributeValueList': [{'S': 'Phones'}], 'ComparisonOperator': 'EQ'}, 'name': { 'AttributeValueList': [{'S': 'app'}], 'ComparisonOperator': 'EQ'}})
        self.dyno_tables['feature_item'].query(name__eq=feature, index='score-index')
        self.dyno.batch_get_item(keys) see keys in searchCategoryFeature..
        self.dyno.scan('item', limit=5, scan_filter={'category': { 'AttributeValueList': [{'S': category}], 'ComparisonOperator': 'EQ'}})
        #dynamo sample query
        dyno.query('feature_item', index_name='item_category-index', limit=2, select='ALL_ATTRIBUTES', key_conditions= {'item_category': { 'AttributeValueList': [{'S': 'Phones'}], 'ComparisonOperator': 'EQ'}, 'name': { 'AttributeValueList': [{'S': 'app'}], 'ComparisonOperator': 'EQ'}})
        '''

        #DynamoDB ish

        self.dyno = boto.dynamodb2.connect_to_region('us-west-2',
                                                     aws_access_key_id='AKIAIZJ2UP4H7M77ON2Q',
                                                     aws_secret_access_key='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')
        self.Items = Table('item_v3', connection=self.dyno)
        self.Features = Table('feature_item_v3', connection=self.dyno)
        self.NegWords = []
        self.MinFeatureMentions = 3
        negFile = 'resources/negative-words.txt'
        try:
            with open(negFile, 'r') as nf:
                self.NegWords = nf.read()
                self.NegWords = self.NegWords.split('\n')
        except:
            print 'Could not open: ', self.negFile

    def test(self):
        print 'This is juss a test...'
        items = self.migrateFeatures()
        print 'test items are: ', len(items)

    def getNonArticlesTimeOrdered(self, lastSeen=None, limit=5):
        items = []
        print 'LIMIT IS: ', limit
        try:
            itemResults = self.Items.query_2(isArticle__eq=0, index='isArticle-time-index', limit=limit, reverse=True, exclusive_start_key=lastSeen)
            for o in list(itemResults):
                item = self.toFrontendObject(o)
                #print 'SCORE TYPE::::::: ', float(item['top2Pos'][0]['score'])
                items.append(item)
            lastSeen = itemResults._last_key_seen
            return {'items': items, 'lastSeen': lastSeen}
        except Exception as err:
            raise
            print 'Some errors happened trying to query items for homeMainContent: ' #, err.value
            return { 'items': items, 'lastSeen': None }

    def getArticlesTimeOrdered(self, limit=16):
        items = []
        try:
            itemsResults = self.Items.query_2(isArticle__eq=1, index='isArticle-time-index', limit=limit, reverse=True)
            for o in list(itemsResults):
                items.append(self.toFrontendObject(o))
        except Exception as err:
            raise
            print 'Some errors occured trying to query items for homeSideContent'
        return items

    def getArticlesByCategory(self, category, limit=16):
        items = []
        try:
            itemsResults = self.Items.query_2(category__eq=category, isArticle__eq=1, index='isArticle-index', limit=limit)
            for o in list(itemsResults):
                items.append(self.toFrontendObject(o))
        except Exception as err:
            raise
            print 'Some errors occured trying to query items for detailsSideContent'
        return items

    def appendFeatures(self, item):
        try:
            featuresToIgnore = ['issu', 'issue', 'problem']
            name = item['name'].strip().lower()
            posResults = self.Features.query_2(itemName__eq=name, score__gt=0, reverse=True, limit=9)
            pos = []
            for p in list(posResults):
                pf = self.toFrontendObject(p)
                print 'SCORE TYPE: ', type(pf['score'])
                if 'pos' in pf:
                    pf['pos'] = pf['pos'][0:12]
                if 'neg' in pf:
                    del pf['neg']
                if 'stem' in pf:
                    if pf['stem'] not in featuresToIgnore:
                        if 'pos' in pf:
                            #only add features with more than 3 reviews mention
                            if len(pf['pos']) > self.MinFeatureMentions:
                                pos.append(pf)
            item['pos'] = pos[0:5]
            negResults = self.Features.query_2(itemName__eq=name, score__lt=0, limit=9)
            neg = []
            for p in list(negResults):
                nf = self.toFrontendObject(p)
                print 'SCORE TYPE: ', type(nf['score'])
                if 'neg' in nf:
                    nf['neg'] = nf['neg'][0:12]
                if 'pos' in nf:
                    del nf['pos']
                if 'stem' in nf:
                    if nf['stem'] not in featuresToIgnore:
                        if 'neg' in nf:
                            #only add features with more than 3 reviews mentions
                            if len(nf['neg']) > self.MinFeatureMentions:
                                neg.append(nf)
            item['neg'] = neg[0:5]

            return item

        except Exception:
            raise
            print 'Some errors happened'
            return {}

    def getItemDetailed(self, name):
        try:
            featuresToIgnore = ['issu', 'issue', 'problem']
            name = name.strip()
            itemQuery = self.Items.query_2(name__eq=name, index='name-index', limit=1)
            item = self.toFrontendObject(itemQuery.next())
            posResults = self.Features.query_2(itemName__eq=name, score__gt=0, reverse=True, limit=9)
            pos = []
            for p in list(posResults):
                pf = self.toFrontendObject(p)
                print 'SCORE TYPE: ', type(pf['score'])
                if 'pos' in pf:
                    pf['pos'] = pf['pos'][0:12]
                if 'neg' in pf:
                    del pf['neg']
                if 'stem' in pf:
                    if pf['stem'] not in featuresToIgnore:
                        if 'pos' in pf:
                            #only add features with more than 3 reviews mention
                            if len(pf['pos']) > self.MinFeatureMentions:
                                pos.append(pf)
            item['pos'] = pos[0:5]
            negResults = self.Features.query_2(itemName__eq=name, score__lt=0, limit=9)
            neg = []
            for p in list(negResults):
                nf = self.toFrontendObject(p)
                print 'SCORE TYPE: ', type(nf['score'])
                if 'neg' in nf:
                    nf['neg'] = nf['neg'][0:12]
                if 'pos' in nf:
                    del nf['pos']
                if 'stem' in nf:
                    if nf['stem'] not in featuresToIgnore:
                        if 'neg' in nf:
                            #only add features with more than 3 reviews mentions
                            if len(nf['neg']) > self.MinFeatureMentions:
                                neg.append(nf)
            item['neg'] = neg[0:5]

            return item

        except Exception:
            raise
            print 'Some errors happened'
            return item

    def getItemDetailedNoLimits(self, name):
        try:
            featuresToIgnore = ['issu', 'issue', 'problem']
            name = name.strip()
            itemQuery = self.Items.query_2(name__eq=name, index='name-index', limit=1)
            item = self.toFrontendObject(itemQuery.next())
            posResults = self.Features.query_2(itemName__eq=name, score__gt=0, reverse=True)
            pos = []
            for p in list(posResults):
                pf = self.toFrontendObject(p)
                #print 'SCORE TYPE: ', type(pf['score'])
                #pf['pos'] = pf['pos'][0:20]
                if 'neg' in pf:
                    del pf['neg']
                if 'stem' in pf:
                    if pf['stem'] not in featuresToIgnore:
                        pos.append(pf)
            item['pos'] = pos
            negResults = self.Features.query_2(itemName__eq=name, score__lt=0)
            neg = []
            for p in list(negResults):
                nf = self.toFrontendObject(p)
                #print 'SCORE TYPE: ', type(pf['score'])
                #nf['neg'] = nf['neg'][0:20]
                if 'pos' in nf:
                    del nf['pos']
                if 'stem' in nf:
                    if nf['stem'] not in featuresToIgnore:
                        neg.append(nf)
                item['neg'] = neg

            return item

        except Exception:
            raise
            print 'Some errors happened'
            return {}

    def getItemWithNoDetails(self, name):
        try:
            featuresToIgnore = ['issu', 'issue', 'problem']
            name = name.strip()
            itemQuery = self.Items.query_2(name__eq=name, index='name-index', limit=1)
            item = itemQuery.next()
            return item

        except Exception:
            raise
            print 'Some errors happened'
            return {}

    def getFeatureByNameAndCat(self, stem, category, itemName):
        try:
            items = self.Features.query_2(stem__eq=stem, category__eq=category, index='stem-category-index')
            for item in items:
                if item['itemName'] == itemName:
                    return item
            return {}
        except Exception as err:
            print 'Some errors occured: ', err.value
            return {}

    def getCategoryByTrend(self, category, limit=13):
        items = []
        try:
            queryResults = self.Items.query_2(category__eq=category, index='trend-index', limit=limit, reverse=True)
            for r in list(queryResults):
                items.append(self.toFrontendObject(r))
            return items
        except Exception:
            print 'Some errors occurred trying to getCategoryByTrend..'
            raise

    def getCategoryByBrand(self, category, brand, limit=6, isStaticPage=False):
        items = []
        try:
            queryResults = self.Items.query_2(category__eq=category, brand__eq=brand, index='brand-category-index', limit=limit)
            for r in list(queryResults):
                item = self.toFrontendObject(r)
                if isStaticPage:
                    item = self.appendFeatures(item)
                items.append(item)
            return items
        except Exception:
            print 'Some errors occurred trying to getCategoryByTrend..'
            raise

    def getCategoryByScore(self, category, limit=6):
        items = []
        try:
            queryResults = self.Items.query_2(category__eq=category, limit=limit, reverse=True)
            for r in list(queryResults):
                items.append(self.toFrontendObject(r))
            return items
        except Exception:
            print 'Some errors occurred trying to getCategoryByTrend..'
            raise

    def addFeature(self, data):
        try:
          self.Features.put_item(data=data)
          return True
        except Exception as err:
          raise
          print 'Some Error occured trying to put item: ', err.value
          return False

    def migrateItems(self):
        items = list(self.Items.query_2(category__eq='test00'))
        for it in items:
            item = {}
            for k in it.keys():
                if k == 'zip':
                    item[k] = str(it[k])
                else:
                    item[k] = it[k]
            self.Items_New.put_item(data=item)
        return items

    def migrateFeatures(self):
        items = list(self.Items.query_2(category__eq='test00'))
        for it in items:
            features = list(self.Features.query_2(itemName__eq=it['name']))
            for feature in features:
                feat = {}
                for f in feature.keys():
                    feat[f] = feature[f]
                self.Features_New.put_item(data=feat)

        return items

    #TODO put all the queries into try except
    def searchCategory(self, category, lastSeenKey=None, limit=5, currentPage=1, isStaticPage=False):
        items = []
        print 'LAST SEEN IS: ', lastSeenKey
        print 'CATEGORY: ', category
        try:
            start = 0
            if currentPage > 1 and lastSeenKey is None:
                #Here we do artificial paging
                print 'ABOUT TO DO SOME ARTIFICIAL PAGING..'
                numRst = currentPage * 5
                start = numRst - (numRst%5)
                if start == numRst:
                    start = numRst - 5
                limit = numRst
                print 'LIMIT IS NOW: ', limit
            itemResults = self.Items.query_2(category__eq=category, exclusive_start_key=lastSeenKey, limit=limit, reverse=True)
            print 'AFTER ITEMRESULTS: ', itemResults
            for o in list(itemResults):
                item = self.toFrontendObject(o)
                if isStaticPage:
                    print 'Its static'
                    item = self.appendFeatures(item)
                items.append(item)
            items = items[start:limit]
            if start == 0:
                lastSeenKey = itemResults._last_key_seen
        except Exception as err:
            raise
            print 'Some errors occured trying to query items for category: ', category
        return {'items': items, 'category': category, 'lastSeenKey': lastSeenKey, 'searchPattern': 'cat'}

    def searchCategoryAll(self, category):
        items = []
        print 'CATEGORY: ', category
        try:
            itemResults = self.Items.query_2(category__eq=category)
            print 'AFTER ITEMRESULTS: ', itemResults
            for o in list(itemResults):
                items.append(self.toFrontendObject(o))
            lastSeenKey = itemResults._last_key_seen
        except Exception as err:
            raise
            print 'Some errors occured trying to query items for category: ', category
        return items

    def searchCategoryUnderPrice(self, category, lastSeenKey=None, limit=10, price=1000, isStaticPage=False):
        items = []
        print 'LAST SEEN IS: ', lastSeenKey
        print 'CATEGORY: ', category
        print 'PRICE: ', price
        try:
            tempLimit = 17
            itemResults = self.Items.query_2(category__eq=category, exclusive_start_key=lastSeenKey, limit=tempLimit, reverse=True)
            print 'AFTER ITEMRESULTS: ', itemResults
            for o in list(itemResults):
                if 'price' in o:
                    if len(items) < limit:
                        if o['price'] <= price:
                            item = self.toFrontendObject(o)
                            if isStaticPage:
                                item = self.appendFeatures(item)
                            items.append(item)
                    else:
                        break
            lastSeenKey = itemResults._last_key_seen
            if len(items) < limit:
                itemResults = self.Items.query_2(category__eq=category, exclusive_start_key=lastSeenKey, limit=tempLimit, reverse=True)
                print 'After querying again to get enough result for the limit AFTER ITEMRESULTS: ', itemResults
                for o in list(itemResults):
                    if len(items) < limit:
                        if 'price' in o:
                            if o['price'] <= price:
                                item = self.toFrontendObject(o)
                                if isStaticPage:
                                    item = self.appendFeatures(item)
                                items.append(item)
                        else:
                            break
        except Exception as err:
            raise
            print 'Some errors occured trying to query items for category: ', category
        return {'items': items, 'category': category, 'lastSeenKey': None, 'searchPattern': 'cat'}

    #TODO sort features(not the item containing the feature prolly gotta use aggreagates) by score and make the searched feature first
    def searchCategoryFeature(self, category, featureStem, adjective=None, lastSeenKey=None, limit=7):
        print 'Category: ',category, ' stemmed feature: ', featureStem, ' adjective: ', adjective
        #dynamo
        #Query on the table so that ResultQuery can handle pagination transparently in the for loop below
        #featureResult = self.dyno_tables['feature_item'].query(stem__eq=featureStem, item_category__eq = category, index='item_category-index', limit=5)
        featureResult = self.Features.query_2(stem__eq=featureStem, category__eq=category, index='stem-category-index',exclusive_start_key=lastSeenKey, limit=limit)
        features = []
        names = []
        for f in featureResult:
            print 'FEATURE NAME: ', f['name']
            if f['category'] == category:
                feat = self.toFrontendObject(f)
                if 'itemName' in f:
                    feat['itemName'] = f['itemName']
                    feat['feature'] = f['name']
                    names.append(f['itemName'])
                features.append(feat)
        names = list(set(names))
        items = []
        for n in names:
            q = self.Items.query_2(name__eq=n, index='name-index')
            item = self.toFrontendObject(q.next())
            featz = [f for f in features if f['itemName'] == item['name']]
            if len(featz) > 0:
                if featz[0]['score'] > 0:
                    item['top2Pos'] = [featz[0]]
                    item['top2Neg'] = []
                elif featz[0]['score'] < 0:
                    item['top2Pos'] = []
                    item['top2Neg'] = [featz[0]]
            items.append(item)

        #TODO use the lexicon to know if an adjective is positive or not
        #TODO order by best feature score first this ordering should be the default and order otherwise if the adjective is negativ
        posItems = []
        negItems = []
        for it in items:
            if 'top2Pos' in it:
                if len(it['top2Pos']) > 0:
                    posItems.append(it)
            if 'top2Neg' in it:
                if len(it['top2Neg']) > 0:
                    negItems.append(it)
        #posItems = [it for it in items if len(it['top2Pos']) > 0]
        #negItems = [it for it in items if len(it['top2Neg']) > 0]
        sortedPosItems = sorted(posItems, key=lambda p:p['top2Pos'][0]['score'])
        sortedNegItems = sorted(negItems, key=lambda p:p['top2Neg'][0]['score'])
        items = sortedNegItems + sortedPosItems
        if adjective is not None:
            if adjective.lower() in self.NegWords:
                #if it's a negative word don't order in a descending order leave as it is..
                pass
            else:
                items.reverse()
        else:
            items.reverse()
        return {'items': items, 'category': category, 'feat': featureStem, 'lastSeenKey': None, 'searchPattern': 'catfeat' }

    def searchBrandCategoryFeature(self, brand, category, featureStem=None, adjective=None, lastSeenKey=None, limit=7):
        print 'Brand: ', brand, 'Category: ',category, ' stemmed feature: ', featureStem, ' adjective: ', adjective
        featureResult = self.Features.query_2(stem__eq=featureStem, category__eq=category, index='stem-category-index',exclusive_start_key=lastSeenKey, limit=limit)
        features = []
        names = []
        for f in featureResult:
            print 'FEATURE NAME: ', f['name']
            if f['category'] == category:
                feat = self.toFrontendObject(f)
                if 'itemName' in f:
                    feat['itemName'] = f['itemName']
                    feat['feature'] = f['name']
                    names.append(f['itemName'])
                features.append(feat)
        names = list(set(names))
        items = []
        for n in names:
            q = self.Items.query_2(name__eq=n, index='name-index')
            item = self.toFrontendObject(q.next())
            if item['brand'] == brand:
                featz = [f for f in features if f['itemName'] == item['name']]
                if len(featz) > 0:
                    if featz[0]['score'] > 0:
                        item['top2Pos'] = [featz[0]]
                        item['top2Neg'] = []
                    elif featz[0]['score'] < 0:
                        item['top2Pos'] = []
                        item['top2Neg'] = [featz[0]]
                items.append(item)

        posItems = [it for it in items if len(it['top2Pos']) > 0]
        negItems = [it for it in items if len(it['top2Neg']) > 0]
        sortedPosItems = sorted(posItems, key=lambda p:p['top2Pos'][0]['score'])
        sortedNegItems = sorted(negItems, key=lambda p:p['top2Neg'][0]['score'])
        items = sortedNegItems + sortedPosItems
        if adjective is not None:
            if adjective.lower() in self.NegWords:
                #if it's a negative word don't order in a descending order leave as it is..
                pass
            else:
                items.reverse()
        else:
            items.reverse()
        return {'items': items, 'brand': brand, 'category': category, 'feat': featureStem, 'lastSeenKey': None, 'searchPattern': 'brandcatfeat' }

    def searchBrandCategory(self, brand, category, lastSeenKey=None, limit=7, currentPage=1, isStaticPage=False):
        print 'Brand: ', brand, 'Category: ', category
        items = []
        brand = brand.lower()
        start = 0
        if currentPage > 1 and lastSeenKey is None:
                #Here we do artificial paging
                print 'ABOUT TO DO SOME ARTIFICIAL PAGING..'
                numRst = currentPage * 7
                start = numRst - (numRst%7)
                if start == numRst:
                    start = numRst - 7
                limit = numRst
                print 'LIMIT IS NOW: ', limit
        if category is not None:
            if isStaticPage:
                results = self.Items.query_2(brand__eq=brand.lower(), category__eq=category, index='brand-category-index', exclusive_start_key=lastSeenKey, limit=limit)
            else:
                results = self.Items.query_2(brand__eq=brand.lower(), category__eq=category, index='brand-index', exclusive_start_key=lastSeenKey, limit=limit)
        else:
            results = self.Items.query_2(brand__eq=brand.lower(), index='brand-score-index', reverse=True, exclusive_start_key=lastSeenKey, limit=limit)

        for item in results:
            item = self.toFrontendObject(item)
            if isStaticPage:
                item = self.appendFeatures(item)
            items.append(item)

        items = items[start:limit]
        if start == 0:
            lastSeenKey = results._last_key_seen

        print 'LAST SEEN is: ', lastSeenKey
        print 'ITEMS LEN is: ', len(items)

        items = sorted(items, key=lambda p:p['score'])
        items.reverse()
        return {'items': items, 'category': category, 'brand': brand, 'lastSeenKey': lastSeenKey, 'searchPattern': 'brandcat' }

    def getItemDetails(self, name):
        name = name.strip()
        itemQuery = self.dyno_tables['item'].query(name__eq=name, limit=1)
        item = itemQuery.next()
        featureResults = self.dyno.query('feature_item', index_name = 'item_name-score-index', limit=20, select='ALL_PROJECTED_ATTRIBUTES', scan_index_forward=False,
                                    key_conditions={ 'item_name': {"ComparisonOperator": "EQ", "AttributeValueList": [{"S": name}] }})
        features = []
        for f in featureResults['Items']:
            feat = {}
            if 'name' in f:
                feat['name'] = f['name'].values()[0].encode('utf-8')
            if 'score' in f:
                feat['score'] = float(f['score'].values()[0])
            if 'neg' in f:
                feat['neg'] = []
                neg = json.loads(f['neg'].values()[0])
                for s in neg:
                    feat['neg'].append(s.encode('utf-8'))
            if 'pos' in f:
                feat['pos'] = []
                pos = json.loads(f['pos'].values()[0])
                for s in pos:
                    feat['pos'].append(s.encode('utf-8'))
            features.append(feat)
        daItem = {}
        if 'name' in item:
                daItem['name'] = item['name'].encode('utf-8')
        if 'score' in item:
                daItem['score'] = float(item['score'])
        if 'imgUrl' in item:
                daItem['imgUrl'] = item['imgUrl'].encode('utf-8')
        daItem['features'] = features
        return daItem

    def _retrieveBatchItemsById(self, itemsIds):
        keys = []
        for v in itemsIds:
            sc = str(v['score'])
            keys.append({'name': {'S': v['name']}, 'score': {'N': sc}})
        kk = {"item": {"Keys": keys, "AttributesToGet": ["name", "score", "features"]}}
        if len(keys) == 0:
            return []
        resp = self.dyno.batch_get_item(kk)
        items = None
        for i in resp:
            if i == 'Responses':
                results = resp['Responses']
                items = results['item']

        items = self._dynoResponseToJson(items)
        return items

    def general_search(self, term):
        """
        This function searches through the db for a given search term.
        For now, we will look at three fields: names, summaries, features
        """

        def getValue(data, key):
            if key in data:
                v = data[key]
                if type(v) is list and len(v):
                    v = "".join(v)
                return v

        print('doing a cloudsearch for: %s' % term)
        # search_endpoint = 'search-reviewizer1-bpgtzrlcu3aoyoqzyk5u2jtysi.us-west-2.cloudsearch.amazonaws.com'
        search_url = 'http://search-reviewizer3-45pk6h6al6mu3qlvttwaxm5epa.us-west-2.cloudsearch.amazonaws.com/2011-02-01/search?q='+\
                     quote(term)+'&return-fields=name,score&rank=-myrank'

        h = Http()
        resp, content = h.request(search_url)
        content = json.loads(content)
        items = []
        keys = []
        if resp['status'] == "200":
            hits = content['hits']
            print('Found items %s' % hits['found'])
            hits = hits['hit']
            for hit in hits:
                d = hit['data']
                keys.append( {'name':str(getValue(d, 'name')), 'score': float(str(getValue(d, 'score')))})
                items.append(
                    {
                        'name': getValue(d, 'name'),
                        'score': float(str(getValue(d, 'score')))
                     })
            # TODO: fix the dynamoDB data. We have a mismatch right now.
            full_items = self._retrieveBatchItemsById(keys)
            # full_items = self._retrieveBatchItemsById(['526c407463d15d03d5dba1c4'])
            return full_items

        return items

    def testSearch(self):
        print 'In dataLayer Module'
        category = 'Phones'
        items = self.mongolab_db[category].find({'numReviews':{'$size': 3}}, {'name': 1, 'features': 1, 'score': 1, 'description': 1})
        return self._toString(items)

    def lastResortSearch(self, q):
        print 'To be implemented.. PROLLY will do text search'

    def _toString(self, items, count=0):
        #TODO make sure the regex works fine
        #Regex to remove the u'' for the unicode strings
        pattern = '([^a-zA-Z0-9]u[^a-zA-Z0-9])'
        tostring = ''
        #Below was a hack to ge around the mongodb ObjectID ting
        for item in items:
            #item['_id'] = str(item['_id'])
            tostring = tostring + ', '+ re.sub(pattern, lambda m: '%s' % m.group(0).replace('u', ''), str(item))
        if len(tostring) > 1:
            tostring = tostring + ',' + '{\'count\': %d}' %count
        return tostring

    def _dynoResponseToJson(self, resp):
        items = []
        for p in resp:
            item = {}
            if 'name' in p:
                item['name'] = p['name'].values()[0]
            if 'id' in p:
                item['id'] = p['id'].values()[0]
            if 'score' in p:
                item['score'] = p['score'].values()[0]
            if 'features' in p:
                if isinstance(p['features'], list):
                    item['features'] = p['features']
                else:
                    item['features'] = p['features'].values()[0]
            if 'description' in p:
                item['description'] = p['description'].values()[0]
            if 'id' in p:
                item['id'] = p['id'].values()[0]
            if 'category' in p:
                item['category'] = p['category'].values()[0]
            if 'item_name' in p:
                item['item_name'] = p['item_name'].values()[0]
            if 'item_category' in p:
                item['item_category'] = p['item_category'].values()[0]
            if 'pos' in p:
                item['pos'] = p['pos'].values()[0]
            if 'neg' in p:
                item['neg'] = p['neg'].values()[0]
            if 'supercategory' in p:
                item['supercategory'] = p['supercategory'].values()[0]
            if 'imgUrl' in p:
                item['imgUrl'] = p['imgUrl'].values()[0]
            if 'videoUrl' in p:
                item['videoUrl'] = p['videoUrl'].values()[0]
            if 'numTalks' in p:
                item['numTalks'] = p['numTalks'].values()[0]

            items.append(item)

        return items

    def compareItems(self, itemA, itemB):
        print('Comparison of items has not been implemented yet!')
        return []

    def toObject(self, botoItem):
        obj = {}
        keys = botoItem.keys()
        for k in keys:
            temp = botoItem[k]
            if isinstance(temp, Decimal):
                obj[k] = float(temp)
            else:
                obj[k] = temp

        return obj

    def toFrontendObject(self, botoItem):
        obj = {}
        keys = botoItem.keys()
        for k in keys:
            temp = botoItem[k]
            if isinstance(temp, Decimal):
                obj[k] = float(temp)
                print 'ITS DECIMAL: ', k, ' VAL: ', obj[k]
            if k == 'score':
                obj[k] = round(float(obj[k]), 1)
                obj['originalScore'] = float(obj[k])
                print 'ORIGINAL SCORE IS: ', obj[k]
            elif k in ['top2Pos', 'top2Neg', 'sources', 'affil', 'pos', 'neg', 'img'] and isinstance(temp, unicode):
                obj[k] = json.loads(temp)
            #elif k in ['top2Pos', 'top2Neg', 'sources', 'affil', 'pos', 'neg', 'img'] and isinstance(temp, list):
                #obj[k] = self.unWrapList(temp)
            elif k in ['top2Pos', 'top2Neg', 'affil', 'pos', 'neg', 'img'] and isinstance(temp, dict):
                obj[k] = self.unWrapList(temp)
                if k in ['top2Pos', 'top2Neg']:
                    for i in range(0, len(obj[k])):
                        obj[k][i]['score'] = round(float(obj[k][i]['score']), 1)
            elif k in ['stem']:
                obj[k] = temp
            elif k == 'sources':
                #obj[k] = temp
                obj[k] = self.unWrapDict(temp)
            elif k == 'name':
                obj[k] = temp.capitalize()
                #shorten name for display in related entitied in details page..
                if len(temp) > 16:
                    obj['relDispName'] = temp[0:18].capitalize() + '..'
                else:
                    obj['relDispName'] = temp.capitalize()
            else:
                obj[k] = temp
        if 'name' in keys:
            if 'category' in keys:
                if 'subCategory' in keys:
                    obj['link'] = '/reviews/' + botoItem['category'] + '/' + botoItem['subCategory'] + '/' + botoItem['name'].replace(' ', '-')
                else:
                    obj['link'] = '/reviews/' + botoItem['category'] + '/' + botoItem['name'].replace(' ', '-')
            else:
                obj['link'] = '/'
        if 'img' in obj.keys():
            if len(obj['img']) and isinstance(obj['img'], list) > 0:
                if isinstance(obj['img'][0], dict):
                    obj['mainImg'] = self.getImgUrlByType(obj['img'], 'large')
                elif isinstance(obj['img'][0], unicode):
                    obj['mainImg'] = obj['img'][0]
            else:
                print 'obj[img] is not a list WTF: ', type(obj['img']), obj['img']
        if 'time' in keys:
            try:
                timeFloat = float(botoItem['time'])
                obj['date'] = str(date.fromtimestamp(timeFloat))
            except Exception as ex:
                obj['date'] = str(date.today())
        else:
            obj['date'] = str(date.today())
        return obj

    def unWrapDict(self, dictObject):
        wrapped = {}
        for k in dictObject:
            if len(k) == 1:
                if isinstance(dictObject[k], dict):
                    return self.unWrapDict(dictObject[k])
                else:
                    return dictObject[k]
            else:
                if isinstance(dictObject[k], dict):
                    wrapped[k] = self.unWrapDict(dictObject[k])
                    if isinstance(wrapped[k], dict):
                        for d in wrapped[k]:
                            if len(d) == 1:
                                if isinstance(wrapped[k][d], dict):
                                    wrapped[k] = self.unWrapDict(wrapped[k][d])
                            else:
                                if isinstance(wrapped[k][d], dict):
                                    wrapped[k][d] = self.unWrapDict(wrapped[k][d])
        return wrapped

    def unWrapList(self, listObject):
        theList = []
        if 'L' in listObject:
            tempList = listObject['L']
            for l in tempList:
                item = {}
                if isinstance(l, dict):
                    theList.append(self.unWrapDict(l))
        return theList

    def wrapList(self, list):
        print 'LIST: ', list
        object = {'L': []}
        for l in list:
            if isinstance(l, dict):
                temp = {'M': {}}
                for k in l.keys():
                    val = l[k]
                    if isinstance(val, Decimal) or isinstance(val, Decimal):
                        temp['M'][str(k)] = {'N': str(val)}
                    elif isinstance(val, str) or isinstance(val, unicode):
                        temp['M'][str(k)] = {'S': str(val)}
                object['L'].append(temp)
        return object

    def getImgUrlByType(self, images, type):
        for img in images:
            if img['imgType'] == type:
                return img['url']
        return ''

if __name__ == '__main__':
    print 'STARTING THE TEST'
    d = DataLayer()
    d.test()
    #datalayer = DataLayer()
    #datalayer.searchCategoryFeature('Phones', 'app')
