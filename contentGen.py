__author__ = 'agasani'
import sys
from searchPatterns import RVTaxonomy
import dataLayer

#TODO merge the below self.Categories with categories in searchPatterns.py into one object that way I don't have to edit 2 places or even better put it in a db
#TODO also make sure I have all brands and categories in the object

class SiteMapGenerator:

    def __init__(self):
        self.SiteMapFile = 'templates/sitemap.txt'
        self.Patterns = ['CATEGORY', 'BRAND CATEGORY with JJ? NP', 'BRAND CATEGORY', 'CATEGORY BRAND', 'BRAND',
                         'CATEGORY with JJ? NP', 'top CD CATEGORY']
        self.SiteMapContent = None
        self.Homepage = 'http://criticjam.com/'
        TX = RVTaxonomy()
        self.Categories = TX.__categories__
        self.Brands = TX.__brands__
        self.datalayer = dataLayer.DataLayer()

    def go(self):
        try:
            with open(self.SiteMapFile, 'w') as sitemap:
                sitemap.write(self.Homepage + '\n')
                qUrl = self.Homepage + 'reviews/q/'
                catUrl = self.Homepage + 'reviews/'
                brandUrl = self.Homepage + 'reviews/b/'
                brandCategoryUrl = self.Homepage + 'reviews/bc/'

                #cat pat
                for cat in self.Categories:
                    cat = cat.replace(' ', '-').lower()
                    sitemap.write(catUrl + cat + '\n')

                #cat feat pat
                for cat in self.Categories.keys():
                    for feat in self.Categories[cat]['features']:
                        sitemap.write(qUrl + cat + '+with+the+best+' + feat + '\n')

                #brand pat
                for brand in self.Brands:
                    brand = brand.replace(' ', '-').lower()
                    sitemap.write(brandUrl + brand + '\n')

                #brand cat
                for brand in self.Brands:
                    for cat in self.Brands[brand]['categories']:
                        brand = brand.replace(' ', '-').lower()
                        cat = cat.replace(' ', '-').lower()
                        sitemap.write(brandCategoryUrl + brand + '/' + cat + '\n')

                #top CD cat
                for cat in self.Categories.keys():
                    sitemap.write(qUrl + 'top+10+' + cat + '\n')

                #top CD cat under CD
                for cat in self.Categories.keys():
                    for price in self.Categories[cat]['pricePoints']:
                        sitemap.write(qUrl + 'top+10+' + cat + '+under+' + '$' + str(price) + '\n')

                #best CAT under CD
                for cat in self.Categories.keys():
                    for price in self.Categories[cat]['pricePoints']:
                        sitemap.write(qUrl + 'best+' + cat + '+under+' + '$' + str(price) + '\n')

                #items
                #cats = set([])
                #subs = set([])
                for category in self.Categories.keys():
                    print 'Category: ', category
                    try:
                        searchResults = self.datalayer.searchCategoryAll(category)
                        for item in searchResults:
                            try:
                                sitemap.write(self.Homepage + 'reviews/' + item['category'].replace(' ', '-') + '/'
                                              + item['subCategory'].replace(' ', '-') + '/' + item['name'].replace(' ', '-') + '\n')
                                #cats.add(item['category'])
                                #subs.add(item['subCategory'])
                            except Exception as ex:
                                print 'ERROR HAPPENED TRYING TO WRITE TO FILE..', ex
                    except Exception as ex:
                        print 'SOME SHITTY ERROR HAPPENED: ', ex

                '''print 'CATEGORIES: '
                print cats
                print 'SUBCATEGORIES: '
                print subs'''

                #TODO np vs np ( this is for later)

                #TODO jj(feature) np under $ (for later)

        except Exception as ex:
            print 'Could not open1: ', self.SiteMapFile, ' Error: ', ex

        try:
            with open(self.SiteMapFile, 'r') as sitemap:
                self.SiteMapContent = sitemap.read()
                print 'The COnTENT is: \n' + self.SiteMapContent
        except:
            print 'Could not open2: ', self.SiteMapFile


if __name__ == '__main__':
    print 'Let\'s generate some content..'
    gen = SiteMapGenerator()
    gen.go()



