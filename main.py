from flask import Flask, url_for, render_template, json, make_response, request, url_for, redirect
from decimal import Decimal
import dataLayer
import search
import time
from datetime import date

DataLayer = dataLayer.DataLayer()
Search = search.QueryInterpretor()

app = Flask(__name__)

@app.route('/reviews/<category>/<subcategory>/<name>')
def getItem(category, subcategory, name):
    #TODO uncomment the lower part after second processing...
    name = name.strip().replace('-', ' ').lower()
    category = category.replace(' ', '-').lower()
    entity = DataLayer.getItemDetailed(name)
    #TODO get the entity, return that and the rest get it thru ajax to make the page load faster..
    #get sideContent as items in same category that are trending
    print 'Category is: ', category, ' and name is: ', name
    #the below will make sense when I start tracking trending entities
    #sideContent = DataLayer.getCategoryByTrend(category)
    #TODO order the below using time modified/added maybe use isArticle-time-index and filter by time or create a new index like category-time-index
    sideContent = DataLayer.getArticlesByCategory(category)
    #Tget related as items in same category and with same brand
    if 'brand' in entity:
        brand = entity['brand']
    else:
        brand = ''

    if len(brand) > 0:
        related = DataLayer.getCategoryByBrand(category, brand)
    else:
        related = DataLayer.getCategoryByScore(category)

    if 'title' in entity:
        title = entity['title']
    else:
        title = entity['name'] + ' Reviews Summary'
    keywords = name + ', ' + category + ', ' + brand + ', ' + ' reviews, opinions, reviews summary, amazon reviews, ' + ', '.join(title.split(' '))
    isEntityPage = True
    contentDescription = 'Reviews Summary of ' + entity['name'] + ' based on ' + str(entity['opNum']) + ' reviews and opinions from ' + ', '.join(entity['sources'].keys())

    print 'CATEGORY IS: ', entity['category']
    if entity['category'] == 'phones':
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=wireless&banner=1FDPWVD9KFPS2JV7CV82&f=ifr&linkID=TC45GNXXANLRRDPB'
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=wireless&banner=12HKFJCNZXPCSGM0MFR2&f=ifr&linkID=LG6N2JCS343HT3JQ'

    elif entity['category'] == 'tablets':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=06CVG03R94GZY11C64R2&f=ifr&linkID=EM7HSMVXBTBXAPVZ'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=0B6D8QB6JH1TZS0ANS02&f=ifr&linkID=QZ37KYRVEZKNIW3K'

    elif entity['category'] == 'ereaders':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=kindlereadingapps&banner=1P3YWVYE8G217EXN4182&f=ifr&linkID=I2QN6P5AUX7ZDOD3'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=cetrade&banner=1PBCP856PS6MDAH7GJ82&f=ifr&linkID=HEAVVBSE462FPTE3'

    elif entity['category'] == 'laptops':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=19KA014JRX72FW0QJ7R2&f=ifr&linkID=5TSOXFS3PIUXYR3K'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=1EE51CNQY8E02W330YR2&f=ifr&linkID=W64YC7NV3VFZGE2G'

    elif entity['category'] == 'smartwatches':
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=cetrade&banner=1PBCP856PS6MDAH7GJ82&f=ifr&linkID=HEAVVBSE462FPTE3'
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=1BT3EK173RVYDV2N8R82&f=ifr&linkID=SPC7NREXAR432IWK'

    elif entity['category'] == 'tvs':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=1T3DR2KGGAG6G3ERBKR2&f=ifr&linkID=W7QV2NIYTC7DZ2QU'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=0PMP8XRJQSEZ3KRPR7R2&f=ifr&linkID=DHTNL54BACM2GVT2'

    elif entity['category'] == 'hard-drives':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=0XAZSFWF5NHP13MSDR02&f=ifr&linkID=AKHKMD4R5FN6MIVM'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=0DC326E2BCGMKDB2X882&f=ifr&linkID=6VT2QAMUBOWLP77H'

    elif entity['category'] == 'cameras':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=camera&banner=1JQWZJX6YHAMT69PQH82&f=ifr&linkID=4L3TTURZ7RSDJUJA'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=camera&banner=0AKPAMNFFGZ8J3JNM1R2&f=ifr&linkID=2CUL6HAYMIA2HBP3'

    elif entity['category'] == 'headphones':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=092YFFR3R72P4WBBBQ02&f=ifr&linkID=O6R4D6DE757CJSZH'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=1CVRH86K14P9G8YEGW82&f=ifr&linkID=4RAWEUAKVRJSKZHL'

    elif entity['category'] == 'desktop-computers':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=15HZBWS1H044BY2VXK82&f=ifr&linkID=JQXAOETXVMWILBI4'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=0X2ZP5AA8SPFVWJ2E3R2&f=ifr&linkID=J66E7PUKEV5CTX4N'

    elif entity['category'] == 'fitness-trackers':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=1BT3EK173RVYDV2N8R82&f=ifr&linkID=SPC7NREXAR432IWK'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=primemain&banner=1N7QZYHSKNC8P06S6QG2&f=ifr&linkID=7MM2FWPNYKNG5UN6'

    elif entity['category'] == 'tv-streaming-sticks':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=firestick&banner=0M4TP05M8D3GEN1ERBG2&f=ifr&linkID=Q6H4NDHNKINBOZ6E'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=firetv&banner=1HZB17ZSN14HN5F95FG2&f=ifr&linkID=ZTNNLXE7RW4AJYWM'


    else:
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=cetrade&banner=1PBCP856PS6MDAH7GJ82&f=ifr&linkID=HEAVVBSE462FPTE3'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=cetrade&banner=1PBCP856PS6MDAH7GJ82&f=ifr&linkID=HEAVVBSE462FPTE3'

    return render_template('product_Raw.html', title=title, entity=entity, sideContent=sideContent, related=related,
                           contentDescription=contentDescription, isEntityPage=isEntityPage, keywords=keywords,
                           ad1Src=ad1Src, ad2Src=ad2Src)

@app.route('/reviews/<category>/<name>')
def getItem_deplicated(category, name):
    print 'IN DUPLICATE..'
    name = name.strip().replace('-', ' ')
    entity = DataLayer.getItemDetailed(name)
    url = 'http://criticjam.com/reviews/' + category + '/' + entity['subCategory'] + '/' + name.replace(' ', '-')
    return redirect(url, code=301)

@app.route('/')
def home():
    pager = paginate()
    pagination = pager['pagination']
    searchFrom = pager['searchFrom']
    currentPage = pager['currentPage']
    morePages = True
    nonArticles = DataLayer.getNonArticlesTimeOrdered(searchFrom)
    lastSeenKey = nonArticles['lastSeen']
    if lastSeenKey is not None:
        lastSeenKey = serializeLastSeenKey(lastSeenKey)
    else:
        morePages = False
    entities = nonArticles['items']
    sideContent = DataLayer.getArticlesTimeOrdered()
    url = url_for('home',reviews='69105')
    title='CriticJam: Simple Reviews Summary: Making better choices quick.'
    contentDescription = 'CriticJam summarizes reviews into insights, wisdom of the crowd, for faster and better choices' \
                         ' among products and services. CriticJam leverages the wisdom of the crowd by aggregating, ' \
                         'analysing and summarizing thousands reviews and various opinions from the internet on sites such as ' \
                         'amazon.com, facebook.com, twitter.com, yelp.com, tripadvisor.com etc.., and it summarizes them ' \
                         'into features people talk about the most and how they feel, sentiment, about them.'

    if lastSeenKey is not None and currentPage > len(pagination):
        pagination.append(lastSeenKey)
    #print 'AFTER adding lastSeen pagination is: ', pagination
    isEntityPage = False
    keywords = 'criticjam, reviews summary, reviews, opinions, amazon reviews'
    resp = make_response(render_template('mainContent.html', title=title, contentDescription=contentDescription,
                                         entities=entities, sideContent=sideContent, pages=len(pagination),
                                         morePages=morePages, url=url, currentPage=currentPage, isEntityPage=isEntityPage, keywords=keywords))
    resp.set_cookie('pg', value=json.dumps(pagination))
    return resp

@app.route('/reviews/<category>')
def getCategory(category):
    print 'IN SEARCH'
    category = category.replace(' ', '-').lower()
    entities = []
    pager = paginate()
    pagination = pager['pagination']
    searchFrom = pager['searchFrom']
    currentPage = pager['currentPage']
    morePages = True
    lastSeenKey = None
    url = url_for('search', q=category)
    feat = None
    searchResults = DataLayer.searchCategory(category, searchFrom)
    if searchResults is not None:
        lastSeenKey = searchResults['lastSeenKey']
        entities = searchResults['items']
        searchPattern = searchResults['searchPattern']
    if lastSeenKey is not None:
        lastSeenKey = serializeLastSeenKey(lastSeenKey)
    else:
        morePages = False
    if lastSeenKey is not None and currentPage > len(pagination):
        pagination.append(lastSeenKey)
    sideContent = DataLayer.getArticlesTimeOrdered()
    title = category.capitalize() + ' Reviews Summary: ' + 'CriticJam'
    contentDescription = 'CriticJam: ' + category.capitalize() + ' reviews summaries based on thousands of reviews and various opinions.'
    keywords = category + ', criticjam, reviews summary, reviews, opinions, amazon reviews'
    resp = make_response(render_template('mainContent.html', title=title, contentDescription=contentDescription,
                                         entities=entities, sideContent=sideContent, pages=len(pagination),
                                         morePages=morePages, currentPage=currentPage, url=url, isSearch=False, query='', keywords=keywords))
    resp.set_cookie('pg', value=json.dumps(pagination))
    resp.set_cookie('searchpattern', value=searchPattern)
    if category is not None:
        resp.set_cookie('cat', value=category)
    if feat is not None:
        resp.set_cookie('feat', value=feat)
    return resp
    '''
    entities = DataLayer.getCategoryByScore(category)
    #TODO use the category-isArticle index to get the is article for the category..
    sideContent = [{'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'}]

    contentDescription = category + ' ordered by CriticJam score from millions of reviews, tweets, facebook posts etc.'
    return render_template('mainContent.html', title=category + ' ranked by reviews\' acclaim --CriticJam.',contentDescription=contentDescription, entities=entities, sideContent=sideContent)
    '''

def doSearch(queri=''):
    print 'IN DOSEARCH'
    entities = []
    searchResults = None
    pager = paginate()
    pagination = pager['pagination']
    searchFrom = pager['searchFrom']
    currentPage = pager['currentPage']
    morePages = True
    lastSeenKey = None
    feat = None
    category = request.cookies.get('cat')
    if queri == '':
        q = request.args.get('q')
    else:
        q = queri
    print 'Query is: ', q
    url = url_for('search', q=q)
    searchPattern = request.cookies.get('searchpattern')
    '''if searchFrom is not None and searchPattern is not None:
        if searchPattern == 'cat' or searchPattern == 'topn':
            category = request.cookies.get('cat')
            searchResults = DataLayer.searchCategory(category, searchFrom)
        elif searchPattern == 'catfeat':
            feat = request.cookies.get('feat')
            searchResults = DataLayer.searchCategoryFeature(category, feat, searchFrom)
    elif q is not None and len(q) > 0:
        searchResults = Search.query(q, searchFrom)'''
    searchResults = Search.query(q, searchFrom)
    if searchResults is not None:
        lastSeenKey = searchResults['lastSeenKey']
        entities = searchResults['items']
        category = searchResults['category']
        searchPattern = searchResults['searchPattern']
        if 'feat' in searchResults:
            feat = searchResults['feat']
    if lastSeenKey is not None:
        lastSeenKey = serializeLastSeenKey(lastSeenKey)
    else:
        morePages = False
    if lastSeenKey is not None and currentPage > len(pagination):
        pagination.append(lastSeenKey)
    sideContent = DataLayer.getArticlesTimeOrdered()
    title = 'Simple Reviews Summary: CriticJam, Making better choices quick.' if q is None else q + ': Simple Reviews Summary: CriticJam, Making better choices quick.'
    isSearch = False if q is None else True
    query = q if q is not None else ''
    contentDescription = q + ' Reviews Summary based on thousands of reviews and various opinions.'
    if category is not None:
        keywords = category + ', ' + q + ', criticjam, reviews summary, reviews, opinions, amazon reviews'
    else:
        keywords = q + ', criticjam, reviews summary, reviews, opinions, amazon reviews'
    resp = make_response(render_template('mainContent.html', title=title, contentDescription=contentDescription,
                                         entities=entities, sideContent=sideContent, pages=len(pagination),
                                         morePages=morePages, url=url, currentPage=currentPage, isSearch=isSearch, query=query, keywords=keywords))
    resp.set_cookie('pg', value=json.dumps(pagination))
    resp.set_cookie('searchpattern', value=searchPattern)
    if category is not None:
        resp.set_cookie('cat', value=category)
    if feat is not None:
        resp.set_cookie('feat', value=feat)
    return resp

@app.route('/reviews')
def search():
    resp = doSearch()
    return resp

@app.route('/reviews/b/<brand>')
def brand(brand):
    brand = brand.replace(' ', '-').lower()
    resp = doSearch(queri=brand)
    return resp

@app.route('/reviews/bc/<brand>/<category>')
def brandCategory(brand, category):
    category = category.replace(' ', '-').lower()
    brand = brand.replace(' ', '-').lower()
    resp = doSearch(queri=brand + ' ' + category)
    return resp

@app.route('/reviews/q/<query>')
def query(query):
    resp = doSearch(queri=query)
    return resp

@app.route('/curatore', methods=['POST', 'GET'])
def curatore():
    method = request.method
    print 'IN CURATORE: the request method is: ', method
    form = request.form
    name = request.args.get('itemName')
    name = name.replace('-', ' ').lower()
    print 'NAME: ', name
    if method.upper() == 'GET':
        if name is not None and len(name) > 0:
            print 'HERE WE GET THE ITEM FOR EDITING..'
            item = DataLayer.getItemDetailedNoLimits(name)
        else:
            print 'NAME IS NONE..'
    else:
        print 'THIS IS POST WE SHOULD UPDATE THE ITEM...'
        item = {}
        itemDyno = DataLayer.getItemWithNoDetails(name)
        category = itemDyno.get('category')
        if itemDyno is not None:
            itemDyno['time'] = str(time.time())
        print 'Category is: ', category
        isArticle = form.get('isArticle')
        print 'ISARTICLE IS: ', isArticle
        #print 'DIR: ', dir(itemDyno)
        #print 'ITEM is: ', itemDyno
        desc = form.get('desc')
        #print 'DESC is: ', desc
        if desc is not None:
            if len(desc) > 7:
                itemDyno['desc'] = desc
        title = form.get('title')
        #print 'TITLE is: ', title
        if title is not None:
            if len(title) > 11:
                print 'TITLE SHOULB UPDATED'
                itemDyno['title'] = title
        if isArticle is not None:
            isArticle = int(isArticle)
            if isArticle == 1 or isArticle == 0:
                print 'ISARTICLE SHOULB UPDATE: ', isArticle
                itemDyno['isArticle'] = isArticle
        stems = form.getlist('featureStem')
        #print 'STEMS: ', stems
        features = form.getlist('featureName')
        #print 'FEATURES: ', features
        scores = form.getlist('featureScore')
        #print 'SCORES: ', scores
        featuresAndScores = []
        if len(features) > 0 and len(features) == len(scores) and len(scores) == len(stems):
            for index, stem in enumerate(stems):
                try:
                    score = scores[index]
                    if score is not None:
                        if len(score) > 0:
                            feature = DataLayer.getFeatureByNameAndCat(stem, category, name)
                            score = Decimal(score)
                            if score != feature['score'] or features[index] != feature['name']:
                                print 'The feature is memory: '
                                newFeature = {}
                                newFeature['score'] = score
                                newFeature['name'] = features[index]
                                print 'the NEW feature name is: ', newFeature['name']
                                newFeature['stem'] = stems[index]
                                print 'NEW STEM is: ', newFeature['stem']
                                newFeature['pos'] = feature['pos']
                                newFeature['neg'] = feature['neg']
                                newFeature['itemName'] = feature['itemName']
                                newFeature['category'] = feature['category']
                                feature.delete()
                                addFeature(newFeature)
                                #newFeature.save()
                            featuresAndScores.append({'feature': features[index], 'score': Decimal(score)})
                            #feature.partial_save()
                except Exception as e:
                    print stem, ' FAILED TO UPDATE: ', e.message
        if len(featuresAndScores) > 0:
            neg = [f for f in featuresAndScores if f['score'] < 0]
            if len(neg) > 0:
                neg = sorted(neg, key=lambda p:p['score'])
                top2Neg = neg[0:2]
                itemDyno['top2Neg'] = top2Neg
            pos = [f for f in featuresAndScores if f['score'] > 0]
            if len(pos) > 0:
                pos = sorted(pos, key=lambda p:p['score'])
                pos.reverse()
                top2Pos = pos[0:2]
                itemDyno['top2Pos'] = top2Pos

        itemDyno.partial_save()

    resp = make_response(render_template('curatore.html', item=item))
    return resp

def addFeature(feature):
    try:
        DataLayer.addFeature(feature)
        print 'TYPE: ', type(feature['score'])
    except Exception as ex:
        print 'Failed to save: ', feature['name'], ' Score: ', feature['score']
        print 'ERROR: ', ex.message
        if feature['score'] < 0:
            feature['score'] = Decimal(str(float(feature['score']) - 0.00001))
        else:
            feature['score'] = Decimal(str(float(feature['score']) + 0.00001))
        addFeature(feature)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('error.html'), 404

@app.errorhandler(500)
def page_not_found(e):
    return render_template('error.html'), 500

@app.route('/BingSiteAuth.xml')
def bingAuth():
    print 'IN BINGAUTH..........................................'
    return render_template('BingSiteAuth.xml')

@app.route('/sitemap.txt')
def sitemap():
    return render_template('sitemap.txt')

@app.route('/test')
def test():
    return render_template('niche_templ.html')

@app.route('/genStaticPage')
def genStaticPage():

    print 'IN DOSEARCH'
    entities = []
    searchResults = None
    pager = paginate()
    pagination = pager['pagination']
    searchFrom = pager['searchFrom']
    currentPage = pager['currentPage']
    morePages = True
    lastSeenKey = None
    feat = None
    category = request.cookies.get('cat')
    limit = request.args.get('limit')
    queri = request.args.get('queri')
    if queri == '':
        q = request.args.get('q')
        cat = request.args.get('cat')
        if cat is not None:
            if cat.lower() != 'all':
                cat = cat.strip().lower()
                q = cat + ' ' + q
    else:
        q = queri
    print 'Query is: ', q
    url = url_for('search', q=q)
    searchPattern = request.cookies.get('searchpattern')
    print 'CURRENT PAGE IS: ', currentPage
    if limit is not None:
        try:
            limit = int(limit)
            print 'LIMIT is: ', limit, ' TYPEOF: ', type(limit)
            searchResults = Search.query(search_term=q, lastSeenKey=searchFrom, currentPage=currentPage, isStaticPage=True, limit=limit)
        except Exception as ex:
            print 'Couldnt convert limit hence no limit'
            searchResults = Search.query(search_term=q, lastSeenKey=searchFrom, currentPage=currentPage, isStaticPage=True)
    else:
        searchResults = Search.query(search_term=q, lastSeenKey=searchFrom, currentPage=currentPage, isStaticPage=True)
    if searchResults is not None:
        lastSeenKey = searchResults['lastSeenKey']
        entities = searchResults['items']
        category = searchResults['category']
        searchPattern = searchResults['searchPattern']
        if 'feat' in searchResults:
            feat = searchResults['feat']
    if lastSeenKey is not None:
        lastSeenKey = serializeLastSeenKey(lastSeenKey)
    else:
        morePages = False
    if lastSeenKey is not None and currentPage > len(pagination):
        pagination.append(lastSeenKey)
    title = request.args.get('title')
    if title is not None:
        keywords = ','.join(title.split(' ')) + ', criticjam, reviews summary, reviews, opinions, amazon reviews'
        title = ' Simple Reviews Summary: CriticJam, Making better choices quick: ' + title
    else:
        if q is not None:
            keywords = q + ', criticjam, reviews summary, reviews, opinions, amazon reviews'
            if category is not None:
                keywords = category + ', ' + q + ', criticjam, reviews summary, reviews, opinions, amazon reviews'
        if category is not None:
            keywords = q + ', criticjam, reviews summary, reviews, opinions, amazon reviews'
    #name eg. 'Hoover vacuums'
    name = request.args.get('name')
    if name is not None:
        contentDescription = name + ' reviews, aggregated and analyzed to show what reviewers like and dislike about them.'
    else:
        contentDescription = "Reviews and opinions, aggregated and analyzed to show what reviewers like and dislike."

    isSearch = False if q is None else True
    query = q if q is not None else ''

    if len(entities) > 0:
        pages = 1
        try:
            pages = int(request.args.get('pg'))
        except Exception as ex:
            print 'Error happened trying to parse pg query string: ', ex.message
        print 'PAGES ARE: ', pages, ' Morepages is: ', morePages, ' and CURRENT page is: ', currentPage

        dat = date.today().isoformat()

        resp = make_response(render_template('niche_templ.html', title=title, description=contentDescription,
                                         isEntityPage=False, keywords=keywords, entities=entities, date=dat, url=url))

        resp.set_cookie('pg', value=json.dumps(pagination))
        resp.set_cookie('searchpattern', value=searchPattern)
        if category is not None:
            resp.set_cookie('cat', value=category)
        if feat is not None:
            resp.set_cookie('feat', value=feat)
        resp.headers['Cache-Control'] = 'public, max-age=8640000'
        return resp
    else:
        if q is None:
            q = 'phone'
        if len(query) == 0:
            q = 'phone'
        resp = make_response(render_template('gSearch.html', title='Search', contentDescription='Search',
                                             pages=1, url='search', isSearch=True, query=q))
        resp.set_cookie('q', value=json.dumps(q))
        return resp


#@@@@@@@@@@@@@@@@@@@@@ EXTENSIONS @@@@@@@@@@@@@@@@@@@@@@@@

def serializeLastSeenKey(lastSeenKey):
    keys = lastSeenKey.keys()
    for k in keys:
        if isinstance(lastSeenKey[k], Decimal):
            lastSeenKey[k] = float(lastSeenKey[k])
    return json.dumps(lastSeenKey)

def deSerializeLastSeenKey(lastSeenKey):
    lastSeenKey = json.loads(lastSeenKey)
    keys = lastSeenKey.keys()
    for k in keys:
        if isinstance(lastSeenKey[k], float):
            lastSeenKey[k] = Decimal(str(lastSeenKey[k]))
    return lastSeenKey

def paginate():
    page = request.args.get('pg')
    print 'PAGE is: ', page
    pagination = []
    searchFrom = None
    pgInt = 1
    if page is not None and page > '1':
        pg = request.cookies.get('pg')
        if pg is not None:
            pagination = json.loads(pg)
            pgInt = int(page)
            if (pgInt-1) <= len(pagination) and (pgInt-2) >= 0:
                searchFrom = deSerializeLastSeenKey(pagination[pgInt-2])
    return {'pagination': pagination, 'searchFrom': searchFrom, 'currentPage': pgInt}

if __name__ == '__main__':
    app.debug = True
    app.run()