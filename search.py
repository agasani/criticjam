__author__ = 'agasani'

from searchPatterns import RVTaxonomy, RVComparisonPattern, RVCategoryPattern, RVSimpleTopNPattern, \
    RVCategoryWithFeaturePattern, RVBrandPatterns, RVSimpleUnderPricePattern
from textblob import TextBlob, Word
import dataLayer
import sys
sys.path.insert(0, 'extensions')
import text2Num

class QueryInterpretor(object):

    def __init__(self, term=None):
        # Setup data
        self.datalayer = dataLayer.DataLayer()

        # Build the taxonomy and make it available globally.
        RVTaxonomy()

        # Set the search term
        self._term = term
        self.standardResultsLimit = 5
        # Setup search patterns.
        self.patterns = {
            'comparison': RVComparisonPattern(),
            'category': RVCategoryPattern(),
            'topN': RVSimpleTopNPattern(),
            'category_with_feature': RVCategoryWithFeaturePattern(),
            'brand_category': RVBrandPatterns(),
            'under_price': RVSimpleUnderPricePattern()
        }

        self.dynoTableNames = RVTaxonomy.__categories__.keys() #['phones', 'cars', 'restaurants', 'hotels', 'books', 'movies', 'laptops', 'smartwatches', 'tvs', 'cameras']

        # Initialize the search
        #return self.query(term)

    def _prepareQueryTerm(self, term):
        term = term.lower()
        if '$' in term:
            index = term.find('$') + 1
            term = term[:index] + ' ' + term[index:len(term)]
        #blob = TextBlob(term)
        #t = blob.correct()
        return term #t.string

    def _prepareCategoryTermForDB(self, category):
        category = category.capitalize()
        b = TextBlob(category)
        category = b.words[0].pluralize().string
        return category.capitalize()

    def _externalize(self, results, term):
        if len(results) > 0:
            #results = DataLayerUtils.externalize(results, count=len(results), query=term)
            print("results: %s" % results)
        return results

    def query(self, search_term=None, lastSeenKey=None, currentPage=1, isStaticPage=False, limit=None):

        if lastSeenKey is None and currentPage > 1:
            #handle artificial pagination
            print 'Should handle artificial paging'

        term = search_term
        if term is None:
            if self._term is not None:
                term = self._term
            else:
                return []

        term = self._prepareQueryTerm(term)
        print('Entered term: %s, Corrected search term: %s' % (search_term, term))
        print 'BEFORE calling patterns query..'
        # TODO: all these patterns should really implement an interface and
        # we can just use an iterator to check them out.

        p = self.patterns['under_price'].query(term)
        print 'P is: ', p
        if p is not None:
            print("under_price Pattern Matched. Keywords: %s" % p)
            print('Start Querying DB..')
            category = p['category'] #self._prepareCategoryTermForDB(p['category'])
            limit = p['n']
            price = float(p['price'])
            print 'price is: ', price
            try:
                limit = int(limit)
            except Exception as ex:
                try:
                    limit = text2Num.text2Num(limit)
                except Exception as ex2:
                    limit = 5
            results = self.datalayer.searchCategoryUnderPrice(category=category, lastSeenKey=lastSeenKey, limit=limit, price=price, isStaticPage=isStaticPage)
            print('Finished querying the DB..')

            return results

        # TODO: remove copy paste code
        p = self.patterns['topN'].query(term)
        if p is not None:
            print("TopN Pattern Matched. Keywords: %s" % p)
            print('Start Querying DB..')
            category = p['category'] #self._prepareCategoryTermForDB(p['category'])
            limit = p['n']
            try:
                limit = int(limit)
            except Exception as ex:
                try:
                    limit = text2Num.text2Num(limit)
                except Exception as ex2:
                    limit = 5
            results = self.datalayer.searchCategory(category=category, lastSeenKey=lastSeenKey, limit=limit, isStaticPage=isStaticPage)
            print('Finished querying the DB..')

            return results

        p = self.patterns['brand_category'].query(term)
        if p is not None:
            print("Brand Category Pattern Matched. Keywords: %s" % p)
            print('Start Querying DB..')
            brand = p['brand']
            category = p['category'] #self._prepareCategoryTermForDB(p['category'])
            adjective = p['adjective'] if 'adjective' in p else None
            feat = p['feature'] if 'feature' in p else None
            print 'BRAND CATEGORY MATCHED#########################'
            if feat is not None and category is not None:
                print 'BOUT TO DO searchBrandCategoryFeature ################## feat: ', feat, ' Category: ', category
                if limit is None:
                    results = self.datalayer.searchBrandCategoryFeature(brand=brand, category=category, featureStem=feat, adjective=adjective, lastSeenKey=lastSeenKey, isStaticPage=isStaticPage)
                else:
                    results = self.datalayer.searchBrandCategoryFeature(brand=brand, category=category, featureStem=feat, adjective=adjective, lastSeenKey=lastSeenKey, isStaticPage=isStaticPage, limit=limit)
            else:
                print 'BOUT TO DO searchBrandCategory #########################'
                if limit is None:
                    results = self.datalayer.searchBrandCategory(brand=brand, category=category, lastSeenKey=lastSeenKey, currentPage=currentPage, isStaticPage=isStaticPage)
                else:
                    results = self.datalayer.searchBrandCategory(brand=brand, category=category, lastSeenKey=lastSeenKey, currentPage=currentPage, isStaticPage=isStaticPage, limit=limit)
            print('Finished querying the DB..')

            return results

        p = self.patterns['category_with_feature'].query(term)
        if p is not None:
            print("Category with feature Pattern Matched. Keywords: %s" % p)
            print('Start Querying DB..')
            category = p['category'] #self._prepareCategoryTermForDB(p['category'])
            adjective = p['adjective'] if 'adjective' in p else None
            results = self.datalayer.searchCategoryFeature(category, p['feature'], adjective, lastSeenKey)
            print('Finished querying the DB..')

            return results

        p = self.patterns['category'].query(term)
        if p is not None:
            print("Category Pattern Matched. Keywords: %s" % p)
            print('Start Querying DB..')
            category = p['category']
            results = self.datalayer.searchCategory(category=category, lastSeenKey=lastSeenKey, currentPage=currentPage, isStaticPage=isStaticPage)
            print('Finished querying the DB..')

            return results

        p = self.patterns['comparison'].query(term)
        if p is not None:
            print("Comparison Pattern Matched. Keywords: %s" % p)
            print('Start Querying DB..')
            results = self.datalayer.compareItems(p['item1'], p['item2'])
            print('Finished querying the DB..')

            return results

if __name__ == "__main__":
    #logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
    #q = raw_input('Enter your search term(s): ')
    k = QueryInterpretor()
    results = k.query('phone with best price')
    print 'FOUND: ', len(results), ' results.'