__author__ = 'agasani'

from flask import Flask, url_for, render_template
import dataLayer

DataLayer = dataLayer.DataLayer()

app = Flask(__name__)


#HomePage test
@app.route('/')
def main():
    entities = []
    entity = {'name': 'iPhone 6 Plus', 'displayName': 'The best Phone on the Market', 'score': 4.8, 'opNum': 20000,
              'pos': [{'feature': 'Screen display', 'score': 4.3}, {'feature': 'Games', 'score': 3.9}],
              'neg': [{'feature': 'Price', 'score': -3.2}, {'feature': 'Size', 'score': -2.9}]}
    for i in range(7):
        entities.append(entity)

    sideContent = [{'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'}]

    contentDescription = 'CriticJam summarizes reviews, wisdom of the crowd, for faster and better choices among products and services. CriticJam leverages the wisdom of the crowd by aggregating, analysing and summarizing reviews and various opinions from the internet on sites such as amazon.com, facebook.com, twitter.com, yelp.com, tripadvisor.com etc.., and it summarizes them into features people talk about the most and how they feel, sentiment, about them.'

    return render_template('mainContent.html', title='CriticJam, simple reviews summarizer.', contentDescription=contentDescription, entities=entities, sideContent=sideContent)

@app.route('/details/')
def details():
    entity = {'name': 'iPhone 6 Plus', 'displayName': 'The best Phone on the Market', 'score': 4.8, 'opNum': 20000,
              'description': 'iPhone 6 Plus is the best smartphone on the market. It has 4.8 CriticJam score based on reviews and other opinions compiled from amazon.com, newegg.com, twitter, facebook and cnet.com \
                                    Apple\'s latest phablet has got rave reviews from users but it\'s not all good. Below are the most talked about things and how people feel about them.',
              'sources': {'Twitter': 1294, 'Facebook': 4213, 'Amazon': 5389, 'BestBuy': 745},
              'images': ['../static/img/sampleimg.png', '../static/img/sampleimg.png', '../static/img/sampleimg.png', '../static/img/sampleimg.png', '../static/img/sampleimg.png'],
              'affiliates': [{'link': 'www.amazon.com', 'price': 799, 'name': 'amazon.com'}, {'link': 'www.pricegrabber.com', 'price': 789, 'name': 'pricegrabber.com'}],
              'pos': [{'feature': 'Screen display', 'score': 4.3}, {'feature': 'Games', 'score': 3.9}, {'feature': 'gesture control', 'score': 3.6},
                      {'feature': 'Games', 'score': 3.9}, {'feature': 'touch screen', 'score': 3.9}],
              'neg': [{'feature': 'apps', 'score': -3.2}, {'feature': 'Size', 'score': -2.9}, {'feature': 'Bed', 'score': -2.7},
                      {'feature': 'Battery Life', 'score': -2.1}, {'feature': 'Size', 'score': -2.9}, {'feature': 'Price', 'score': -2.9},
                      {'feature': 'Microsd', 'score': -1.9}]}

    sideContent = [{'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'}]

    related = [{'name': 'Nexus 6', 'score': 4.6, 'link': 'details/top_10_smartphones/123', 'image': '../static/img/sampleimg.png', 'oNum': 5831},
               {'name': 'Nexus 6', 'score': 4.6, 'link': 'details/top_10_smartphones/123', 'image': '../static/img/sampleimg.png', 'oNum': 5831},
               {'name': 'Nexus 6', 'score': 4.6, 'link': 'details/top_10_smartphones/123', 'image': '../static/img/sampleimg.png', 'oNum': 5831},
               {'name': 'Nexus 6', 'score': 4.6, 'link': 'details/top_10_smartphones/123', 'image': '../static/img/sampleimg.png', 'oNum': 5831},
               {'name': 'Nexus 6', 'score': 4.6, 'link': 'details/top_10_smartphones/123', 'image': '../static/img/sampleimg.png', 'oNum': 5831},
               {'name': 'Nexus 6', 'score': 4.6, 'link': 'details/top_10_smartphones/123', 'image': '../static/img/sampleimg.png', 'oNum': 5831}]

    title = entity['displayName']  if len(entity['displayName']) > 0 else entity['name'] + ' Reviews Summary'
    contentDescription = 'Summary of ' + str(entity['opNum']) + ' reviews and opinions about ' + entity['name'] + ' from ' + ', '.join(entity['sources'].keys())

    return render_template('product.html', title=title, entity=entity, sideContent=sideContent, related=related, contentDescription=contentDescription)


@app.route('/reviews/<category>')
def getCategory(category):
    category = category.lower().capitalize()
    print 'the category is: ', category
    entities = []
    testEntity = DataLayer.searchCategory(category, limit=7)
    for entity in testEntity:
        print 'Phone name is: ', entity['name'], ' score: ', entity['score']
        temp = {}
        temp['displayName'] = entity['title'] if 'title' in entity else entity['name']
        temp['link'] = 'reviews/' + category + '/' + entity['name'].replace(' ', '-')
        temp['name'] = entity['name']
        temp['opNum'] = entity['numTalks']
        temp['score'] = entity['score']
        #TODO find a way to get the top 2 pos and neg features with their scores, prolly have a variation of searchCategory method..
        temp['top2Pos'] = [{'feature': 'Screen display', 'score': 4.3}, {'feature': 'Games', 'score': 3.9}]
        temp['top2Neg'] = [{'feature': 'Price', 'score': -3.2}, {'feature': 'Size', 'score': -2.9}]

        entities.append(temp)

    sideContent = [{'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'},
                   {'title': 'Top 10 Smartphones', 'link': 'details/top_10_smartphones/123'},
                   {'title': 'The Hotel with the Best View in Las Vegas', 'link': 'details/hotel_best_view_las_vegas/123'}]

    contentDescription = category + ' ordered by CriticJam score from millions of reviews, tweets, facebook posts etc.'
    return render_template('mainContent.html', title=category + ' ranked by reviews\' acclaim --CriticJam.',contentDescription=contentDescription, entities=entities, sideContent=sideContent)



