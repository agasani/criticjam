__author__ = 'agasani'

import nltk
#import sys
#sys.path.append('/usr/local/lib/python2.7/site-packages/pattern')
from pattern.en import parsetree as TreeParser
from pattern.search import search, Pattern, Constraint, taxonomy, match

porter = nltk.PorterStemmer()

class RVTaxonomy(object):

    __categories__ = {
        #'car': {
            #'synonyms': ['automotive', 'automotives', 'vehicle', 'vehicles', 'truck', 'trucks', 'ride', 'rides'],
            #'features': ['dependable'],
            #'pricePoints': [3000, 5000, 10000, 12000, 15000, 20000]
            #},
        'phones': {
            'synonyms': ['phone', 'cellphone', 'cellphones', 'cell-phone', 'cell-phones', 'cellular', 'smartphone', 'smartphones',
                   'telephone', 'telephones', 'mobile', 'mobiles', 'hand-held-device', 'hand-held-devices'],
            'features': ['screen', 'display', 'size', 'camera', 'battery', 'apps', 'features', 'price'],
            'pricePoints': [100, 300, 500]
            },
        'tablets': {
            'synonyms': ['tablet','pad', 'pads'],
            'features': ['screen', 'display', 'size', 'camera', 'battery', 'app', 'feature', 'price'],
            'pricePoints': [100, 400]
            },
        'cameras': {
            'synonyms': ['camera', 'digital-camera', 'digital-cameras', 'dslr-camera', 'dslr-cameras', 'slr-camera', 'slr-camera',
                    'point and shoot camera', 'point-and-shoot-cameras'],
            'features': ['pictures', 'shots', 'price', 'lenses', 'features'],
            'pricePoints': [100, 500]
            },
        'laptops': {
            'synonyms': ['laptop', 'portable-computer', 'portable-computers'],
            'features': ['price', 'battery', 'screen', 'display', 'keyboard'],
            'pricePoints': [300, 500, 1000]
            },
        'smartwatches': {
            'synonyms': ['smartwatch', 'ewatch', 'ewatches', 'electronic-watch', 'electronic-watches', 'watch'],
            'features': ['apps', 'price', 'notifications', 'battery', 'screen', 'features', 'display', 'face'],
            'pricePoints': [50, 200]
            },
        'desktop-computers': {
            'synonyms': ['desktop-computer', 'computer', 'computers', 'desktop', 'desktops', 'pc', 'pcs'],
            'features': ['price', 'performance', 'size', 'screen', 'speakers', 'monitor'],
            'pricePoints': [300, 500, 1000]
            },
        'headphones': {
            'synonyms': ['headphone', 'earphone', 'earphones', 'earbud', 'earbuds'],
            'features': ['sound', 'price', 'bass', 'design'],
            'pricePoints': [50, 100, 200]
            },
        'external-hard-drives': {
            'synonyms': ['external-hard-drive', 'harddrive', 'harddrives', 'hard-drive', 'hard-drives', 'hd'],
            'features': ['capacity', 'price', 'storage'],
            'pricePoints': [50, 100, 200]
            },
        'tvs': {
            'synonyms': ['tv', 'tv-set', 'tv-sets', 'television', 'televisions', 'television-set', 'television-sets', 'smart-tv', 'smart-tvs', 'smarttv', 'smarttvs'],
            'features': ['resolution', 'price', 'size', 'pictures', 'speakers'],
            'pricePoints': [300, 500, 1000]
            },
        'ereaders': {
            'synonyms': ['ereader', 'book-reader', 'book-readers', 'e-reader', 'e-readers', 'ebook-reader', 'ebook-readers'],
            'features': ['price', 'screen', 'games', 'apps', 'camera', 'resolution', 'battery'],
            'pricePoints': [100, 200]
            },
        'tv-streaming-sticks': {
            'synonyms': ['tv-streaming-stick', 'tv-streaming-media', 'streamer', 'streamers', 'tv-streaming', 'tv-streaming-box', 'tv-streaming-boxes'],
            'features': ['hdmi'],
            'pricePoints': [50, 100, 200]
            },
        'fitness-trackers': {
            'synonyms': ['fitness-tracker', 'fitness-band','fitness-bands', 'fitness-bracelet', 'fitness-bracelets',
                             'fitness-tracker', 'fitness-trackers', 'tracker', 'trackers'],
            'features': ['price', 'coaching', 'tracker activity', 'looks', 'features', 'battery'],
            'pricePoints': [50, 100, 200]
            },
        #'restaurants': {
            #'synonyms':['restaurant', 'eatery', 'eating-place', 'bistro'],
            #'features': ['food'],
            #'pricePoints': []
        #},
        #'hotels': {
            #'synonyms': ['hotel', 'place to stay', 'inn'],
            #'features': ['location', 'view', 'rooms'],
            #'pricePoints': []
        #},
        #'books': {
            #'synonyms': ['book', 'thing to read', 'story'],
            #'features': ['read', 'story', 'plot'],
            #'pricePoints': []
        #},
        #'movies': {
            #'synonyms': ['movies', 'video', 'film', 'thing-to-watch', 'stuff-to-watch'],
            #'features': ['story', 'plot'],
            #'pricePoints': []
        #},
        'garbage-disposals':{
            'synonyms': ['garbage-disposal', 'garbage-disposers', 'garbage-disposer', 'disposal', 'disposals', 'disposers', 'disposer'],
            'features': ['price', 'value'],
            'pricePoints': []
        },
        'power-tools':{
            'synonyms': ['power-tool', 'tools', 'tool', 'equipment'],
            'features': ['price', 'value'],
            'pricePoints': []
        },
        'hair-loss-treatment':{
            'synonyms': ['hair-loss', 'hair-product', 'hair-products', 'baldness-cure'],
            'features': ['result', 'price', 'detangler', 'growth'],
            'pricePoints': []
        },
        'power-generators':{
            'synonyms': ['generators', 'generator', 'power-generator', 'electric-generator', 'electric-generators'],
            'features': [],
            'pricePoints': []
        },
        'solar-panels':{
            'synonyms': ['solar-panel', 'photovoltaic-system', 'photovoltaic-systems'],
            'features': [],
            'pricePoints': []
        },
        'photography-drones':{
            'synonyms': ['photography-drone', 'drones', 'drone'],
            'features': [],
            'pricePoints': []
        },
        'recreational-drones':{
            'synonyms': ['recreational-drone', 'drones', 'drone'],
            'features': [],
            'pricePoints': []
        },
        'camping-tents':{
            'synonyms': ['camping-tent', 'tent', 'tents', 'shelter', 'shelters'],
            'features': [],
            'pricePoints': []
        },
        'vacuum-cleaners':{
            'synonyms': ['vacuum-cleaner', 'vacuums', 'vacuum', 'floor-cleaners', 'floor-cleaner', 'carpet-cleaners', 'carpet-cleaner'],
            'features': ['lightweight', 'suction', 'hose'],
            'pricePoints': []
        }
    }

    __dynoTableNames__ = ['phone', 'car', 'restaurant', 'hotel', 'book', 'movie']
    __electronics__ = ['phones', 'tablets', 'laptops', 'cameras', 'smartwatches', 'computers', 'ereaders', 'tvs', 'external-hard-drive',
                       'fitness-trackers', 'tv-streaming-sticks', 'desktop-computers']
    __phones__ = ['iphone', 'samsung galaxy', 'htc', 'blackberry']
    __superCategories__ = ['car', 'electronic', 'restaurant', 'hotel', 'book', 'consumer', 'movie']
    __brands__ = {
                'apple': {
                    'categories': ['phones', 'desktop-computers', 'tablets', 'smartwatches', 'laptops', 'tv-streaming-sticks']
                },
                'samsung': {
                    'categories': ['tvs', 'phones', 'tablets', 'smartwatches', 'laptops']
                },
                'lg': {
                    'categories': ['tvs', 'phones']
                },
                'htc': {
                    'categories': ['phones']
                },
                'sony': {
                    'categories': ['phones', 'tvs', 'laptops', 'headphones', 'smartwatches']
                },
                'nokia': {
                    'categories': ['phones']
                },
                'oneplus': {
                    'categories': ['phones']
                },
                'amazon': {
                    'categories': ['tablets', 'ereaders', 'tv-streaming-sticks']
                },
                'google': {
                    'categories': ['tv-streaming-sticks', 'phones']
                },
                'motorola': {
                    'categories': ['phones']
                },
                'western-digital': {
                    'categories': ['external-hard-drives']
                },
                'toshiba': {
                    'categories': ['laptops', 'exteranl-hard-drives']
                },
                'seagate': {
                    'categories': ['exteranal-hard-drives']
                },
                'skullcandy': {
                    'categories': ['headphones']
                },
                'canon': {
                    'categories': ['cameras']
                },
                'nikon': {
                    'categories': ['cameras']
                },
                'audio-technica':{
                    'categories': ['headphones']
                },
                'seiki':{
                    'categories': ['tvs']
                },
                'seagate':{
                    'categories': ['external-hard-drives']
                },
                'blackberry':{
                    'categories': ['phones']
                },
                'vizio':{
                    'categories': ['tvs']
                },
                'padgene':{
                    'categories': ['smartwatches']
                },
                'microsoft':{
                    'categories': ['tablets']
                },
                'panasonic':{
                    'categories': ['headphones']
                },
                'philips':{
                    'categories': ['headphones']
                },
                'upstar':{
                    'categories': ['tvs']
                },
                'symphonized':{
                    'categories': ['headphones']
                },
                'roku':{
                    'categories': ['tv-streaming-sticks']
                },
                'tivo':{
                    'categories': ['tv-streaming-sticks']
                },
                'sceptre':{
                    'categories': ['tvs']
                },
                'element':{
                    'categories': ['tvs']
                },
                'moov':{
                    'categories': ['fitness-trackers']
                },
                'lifetrak':{
                    'categories': ['fitness-trackers']
                },
                'kobo':{
                    'categories': ['ereaders']
                },
                'koss':{
                    'categories': ['headphones']
                },
                'sentey':{
                    'categories': ['headphones']
                },
                'lenovo':{
                    'categories': ['laptops']
                },
                'fitbit':{
                    'categories': ['fitness-trackers']
                },
                'jawbone':{
                    'categories': ['fitness-trackers']
                },
                'timex':{
                    'categories': ['fitness-trackers']
                },
                'luxsure':{
                    'categories': ['smartwatches']
                },
                'cyberpower':{
                    'categories': ['desktop-computers']
                },
                'polar':{
                    'categories': ['fitness-trackers']
                },
                'sennheiser':{
                    'categories': ['headphones']
                },
                'acer':{
                    'categories': ['laptops']
                },
                'acer':{
                    'categories': ['laptops', 'desktop-computers']
                },
                'hp':{
                    'categories': ['laptops', 'desktop-computers']
                },
                'silicon-power':{
                    'categories': ['external-hard-drives']
                },
                'hgst':{
                    'categories': ['external-hard-drives']
                },
                'dji':{
                  'categories': ['drones']
                },
                '5ive':{
                    'categories': ['smartwatches']
                },
                'hoover':{
                    'categories': ['vacuum-cleaners']
                },
                'shark':{
                    'categories': ['vacuum-cleaners']
                },
                'electrolux':{
                    'categories': ['vacuum-cleaners']
                },
                'kirby':{
                    'categories': ['vacuum-cleaners']
                },
                'dirt-devil':{
                    'categories': ['vacuum-cleaners']
                }
            }

    def __init__(self):
        self._buildTaxonomy()

    def _buildTaxonomy(self):
        '''
        Build a taxonomy. We should load this from a config file or db. for now, this will do.
        TODO: are synonyms supposed to be child of their corresponding word ?
        '''
        for i in self.__electronics__:
            taxonomy.append(i, type="electronic")
        for i in self.__phones__:
            taxonomy.append(i, type="phone")
        for cat in self.__categories__.keys():
            for sub in self.__categories__[cat]['synonyms']:
                taxonomy.append(sub, type=cat)
        for superCat in self.__superCategories__:
           taxonomy.append(superCat, type="category")
        for cat in self.__categories__.keys():
            taxonomy.append(cat, type='category')
        for brand in self.__brands__.keys():
            taxonomy.append(brand, 'brand')
        taxonomy.append('category', type="super")

    def rvTaxonomy(self):
        return taxonomy

class RVAbstractBasePattern(object):
    def __init__(self, term=None):
        self._term = term
        self.categories = RVTaxonomy().__categories__
    def getCategory(self, cat):
        if cat is not None:
            if cat not in self.categories.keys():
                for key in self.categories.keys():
                    if cat in self.categories[key]['synonyms']:
                        cat = key
                        break
        return cat

class RVCategoryPattern(RVAbstractBasePattern):
    __patterns__ = [Pattern.fromstring('CATEGORY')]

    def __init__(self, term=None):
        super(RVCategoryPattern, self).__init__(term)

    def query(self, term):
        print "term: ", term
        term = term.replace(' ', '-')
        pattern = self.__patterns__[0]
        parsedoc = TreeParser(term, lemmata=True)
        m = pattern.search(parsedoc)
        if m:
            m = pattern.match(parsedoc)
            r = {}
            for w in m.words:
                if str(m.constraint(w)) == 'Constraint(taxa=[\'category\'])':
                    r['category'] = self.getCategory(w.string)
                    r['parentCategories'] = taxonomy.parents(w.string, recursive=True)
            return r
        return None

class RVBrandPatterns(RVAbstractBasePattern):
    __patterns__ = ['BRAND CATEGORY with JJ? NP', 'CATEGORY BRAND with JJ? NP', 'BRAND CATEGORY NP', 'CATEGORY BRAND NP', 'BRAND CATEGORY', 'CATEGORY BRAND', 'BRAND']

    def __init__(self, term=None):
        super(RVBrandPatterns, self).__init__(term)

    def query(self, term):
        print 'TERM is: ', term
        s = TreeParser(term, lemmata=True)
        for p in self.__patterns__:
            m = search(p, s)
            if m:
                print p, ' MATCHED...............................'
                m = match(p, s)
                r = {}
                nps = []
                adj = None
                feature = None
                category = None
                parentCategories = None
                for w in m.words:
                    print 'm CONTRAINT: ', w.string, ' TAG: ', w.tag, ' ', str(m.constraint(w))
                    if str(m.constraint(w)) == 'Constraint(taxa=[\'category\'])':
                        category = w.string
                        parentCategories = taxonomy.parents(w.string, recursive=True)
                    if str(m.constraint(w)) == 'Constraint(taxa=[\'brand\'])':
                        r['brand'] = w.string
                        print 'BRAND IS############################## ', w.string
                    if str(m.constraint(w)) == 'Constraint(chunks=[\'NP\'])' and (w.tag == 'NN' or w.tag == 'NNS'):
                        #print 'The constraint for..', w.string, m
                        nps.append(w.string)
                        print 'FEATURE IS############################## ', w.string
                    if str(m.constraint(w)) == 'Constraint(chunks=[\'NP\'])' and (w.tag == 'JJ' or w.tag == 'JJS'):
                        adj = w.string
                stems = []
                for n in nps:
                    stems.append(porter.stem(n))
                if len(stems) > 0:
                    feature = ' '.join(x for x in stems)

                r['feature'] = feature
                r['adjective'] = adj
                r['category'] = self.getCategory(category)
                r['parentCategories'] = parentCategories

                return r
        return None

class RVCategoryWithFeaturePattern(RVAbstractBasePattern):
    __patterns__ = ['CATEGORY with JJ? NP', 'CATEGORY NP']

    def __init__(self, term=None):
        super(RVCategoryWithFeaturePattern, self).__init__(term)

    def query(self, term):
        s = TreeParser(term, lemmata=True)
        for p in self.__patterns__:
            m = search(p, s)
            if m:
                m = match(p, s)
                r = {}
                nps = []
                adj = None
                for w in m.words:
                    #print 'm CONTRAINT: ', w.string, ' TAG: ', w.tag, ' ', str(m.constraint(w))
                    if str(m.constraint(w)) == 'Constraint(taxa=[\'category\'])':
                        r['category'] = self.getCategory(w.string)
                        r['parentCategories'] = taxonomy.parents(w.string, recursive=True)
                    if str(m.constraint(w)) == 'Constraint(chunks=[\'NP\'])' and (w.tag == 'NN' or w.tag == 'NNS'):
                        #print 'The constraint for..', w.string, m
                        nps.append(w.string)
                    if str(m.constraint(w)) == 'Constraint(chunks=[\'NP\'])' and (w.tag == 'JJ' or w.tag == 'JJS'):
                        adj = w.string
                stems = []
                for n in nps:
                    stems.append(porter.stem(n))
                r['feature'] = ' '.join(x for x in stems)
                r['adjective'] = adj
                return r
        return None

class RVComparisonPattern(RVAbstractBasePattern):
    __patterns__ = ['NP vs* NP', 'NP compare with NP']

    def __init__(self, term=None):
        super(RVComparisonPattern, self).__init__(term)

    def query(self, term):
        s = TreeParser(term, lemmata=True)
        for p in self.__patterns__:
            m = search(p, s)
            if m:
                m = match(p, s)
                print("we have comparison search: %s" % m)

                # TODO: complete this.
                for w in m.words:
                    #print(str(m.constraint(w)))
                    pass

                return {}

class RVSimpleTopNPattern(RVAbstractBasePattern):
    __patterns__ = ['top CD CATEGORY']

    def __init__(self, term=None, *args, **kwargs):
        super(RVSimpleTopNPattern, self).__init__(term)

    def query(self, term):
        s = TreeParser(term, lemmata=True)
        for p in self.__patterns__:
            m = search(p, s)
            if m:
                m = match(p, s)
                nps = []
                for w in m.words:
                    if str(m.constraint(w)) == 'Constraint(taxa=[\'category\'])':
                        cat = w.string
                        parentCategories = taxonomy.parents(w.string, recursive=True)
                    if str(m.constraint(w)) == 'Constraint(chunks=[\'NP\'])':
                        nps.append(w.string)
                    if str(m.constraint(w)) == 'Constraint(tags=[\'CD\'])':
                        n = w.string
                stems = []
                for n in nps:
                    stems.append(porter.stem(n))
                feature = ' '.join(x for x in stems)
                cat = self.getCategory(cat)
                return {'category': cat, 'n': n, 'feature': feature, 'parentCategories': parentCategories}
        return None

class RVSimpleUnderPricePattern(RVAbstractBasePattern):
    __patterns__ = ['top CD CATEGORY under $? CD', 'best CATEGORY under $? CD']

    def __init__(self, term=None, *args, **kwargs):
        super(RVSimpleUnderPricePattern, self).__init__(term)

    def query(self, term):
        s = TreeParser(term, lemmata=True)
        for i, p in enumerate(self.__patterns__):
            m = search(p, s)
            if m:
                m = match(p, s)
                nps = []
                n = ''
                price = '1000'
                for w in m.words:
                    if str(m.constraint(w)) == 'Constraint(taxa=[\'category\'])':
                        cat = w.string
                        parentCategories = taxonomy.parents(w.string, recursive=True)
                    if str(m.constraint(w)) == 'Constraint(chunks=[\'NP\'])':
                        nps.append(w.string)
                    if str(m.constraint(w)) == 'Constraint(tags=[\'CD\'])':
                        if i == 0:
                            if n == '':
                                n = w.string
                            else:
                                price = w.string
                        elif i == 1:
                            price = w.string
                stems = []
                for n in nps:
                    stems.append(porter.stem(n))
                feature = ' '.join(x for x in stems)
                cat = self.getCategory(cat)
                return {'category': cat, 'n': n, 'price': price, 'feature': feature, 'parentCategories': parentCategories}

        return None
