from flask import Flask, url_for, render_template, json, make_response, request, url_for, redirect
from decimal import Decimal
import dataLayer
import search

DataLayer = dataLayer.DataLayer()
Search = search.QueryInterpretor()

application = Flask(__name__)


@application.route('/reviews/<category>/<subcategory>/<name>')
def getItem(category, subcategory, name):
    #TODO uncomment the lower part after second processing...
    name = name.strip().replace('-', ' ') .lower()
    entity = DataLayer.getItemDetailed(name)
    #TODO get the entity, return that and the rest get it thru ajax to make the page load faster..
    #get sideContent as items in same category that are trending
    print 'Category is: ', category, ' and name is: ', name
    #the below will make sense when I start tracking trending entities
    #sideContent = DataLayer.getCategoryByTrend(category)
    #TODO order the below using time modified/added maybe use isArticle-time-index and filter by time or create a new index like category-time-index
    sideContent = DataLayer.getArticlesByCategory(category)
    #Tget related as items in same category and with same brand
    if 'brand' in entity:
        brand = entity['brand']
    else:
        brand = ''

    if len(brand) > 0:
        related = DataLayer.getCategoryByBrand(category, brand)
    else:
        related = DataLayer.getCategoryByScore(category)

    if 'title' in entity:
        title = entity['title']
    else:
        title = entity['name'] + ' Reviews Summary'
    keywords = name + ', ' + category + ', ' + brand + ', ' + ' reviews, opinions, reviews summary, amazon reviews, ' + ', '.join(title.split(' '))
    isEntityPage = True
    contentDescription = 'Reviews Summary of ' + entity['name'] + ' based on ' + str(entity['opNum']) + ' reviews and opinions from ' + ', '.join(entity['sources'].keys())

    print 'CATEGORY IS: ', entity['category']
    if entity['category'] == 'phones':
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=wireless&banner=1FDPWVD9KFPS2JV7CV82&f=ifr&linkID=TC45GNXXANLRRDPB'
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=wireless&banner=02HND5YJK5NEFPBWFS02&f=ifr&linkID=KC3WHRDNOIE5ADTT'

    elif entity['category'] == 'tablets':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=06CVG03R94GZY11C64R2&f=ifr&linkID=EM7HSMVXBTBXAPVZ'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=0B6D8QB6JH1TZS0ANS02&f=ifr&linkID=QZ37KYRVEZKNIW3K'

    elif entity['category'] == 'ereaders':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=kindlereadingapps&banner=1P3YWVYE8G217EXN4182&f=ifr&linkID=I2QN6P5AUX7ZDOD3'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=cetrade&banner=1PBCP856PS6MDAH7GJ82&f=ifr&linkID=HEAVVBSE462FPTE3'

    elif entity['category'] == 'laptops':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=19KA014JRX72FW0QJ7R2&f=ifr&linkID=5TSOXFS3PIUXYR3K'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=1EE51CNQY8E02W330YR2&f=ifr&linkID=W64YC7NV3VFZGE2G'

    elif entity['category'] == 'smartwatches':
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=cetrade&banner=1PBCP856PS6MDAH7GJ82&f=ifr&linkID=HEAVVBSE462FPTE3'
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=1BT3EK173RVYDV2N8R82&f=ifr&linkID=SPC7NREXAR432IWK'

    elif entity['category'] == 'tvs':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=1T3DR2KGGAG6G3ERBKR2&f=ifr&linkID=W7QV2NIYTC7DZ2QU'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=0PMP8XRJQSEZ3KRPR7R2&f=ifr&linkID=DHTNL54BACM2GVT2'

    elif entity['category'] == 'hard-drives':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=0XAZSFWF5NHP13MSDR02&f=ifr&linkID=AKHKMD4R5FN6MIVM'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=0DC326E2BCGMKDB2X882&f=ifr&linkID=6VT2QAMUBOWLP77H'

    elif entity['category'] == 'cameras':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=camera&banner=1JQWZJX6YHAMT69PQH82&f=ifr&linkID=4L3TTURZ7RSDJUJA'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=camera&banner=0AKPAMNFFGZ8J3JNM1R2&f=ifr&linkID=2CUL6HAYMIA2HBP3'

    elif entity['category'] == 'headphones':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=092YFFR3R72P4WBBBQ02&f=ifr&linkID=O6R4D6DE757CJSZH'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=1CVRH86K14P9G8YEGW82&f=ifr&linkID=4RAWEUAKVRJSKZHL'

    elif entity['category'] == 'desktop-computers':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=15HZBWS1H044BY2VXK82&f=ifr&linkID=JQXAOETXVMWILBI4'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=0X2ZP5AA8SPFVWJ2E3R2&f=ifr&linkID=J66E7PUKEV5CTX4N'

    elif entity['category'] == 'fitness-trackers':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=computers_accesories&banner=1BT3EK173RVYDV2N8R82&f=ifr&linkID=SPC7NREXAR432IWK'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=primemain&banner=1N7QZYHSKNC8P06S6QG2&f=ifr&linkID=7MM2FWPNYKNG5UN6'

    elif entity['category'] == 'tv-streaming-sticks':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=firestick&banner=0M4TP05M8D3GEN1ERBG2&f=ifr&linkID=Q6H4NDHNKINBOZ6E'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=firetv&banner=1HZB17ZSN14HN5F95FG2&f=ifr&linkID=ZTNNLXE7RW4AJYWM'

    elif entity['category'] == 'garbage-disposals':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=tools&banner=08RES0FRK165XBC3J182&f=ifr&linkID=HGGZIGSZNDNEA5P7'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=tools&banner=15Z6CMNTERDHJDJA95G2&f=ifr&linkID=2IUYLD37TGYFQXUO'

    elif entity['category'] == 'power-tools':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=tools&banner=0RW4XSHS68YADKQ2TXR2&f=ifr&linkID=X2FOJVT3GH5DRCHL'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=tools&banner=05VW7W09074S9VWGVB02&f=ifr&linkID=36HOPWNBE7JHMYWV'

    elif entity['category'] == 'hair-loss-treatment':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=beauty_editorspicks&banner=1A5SHYGPKCSPBAZ2PF02&f=ifr&linkID=OL7ZXCX6PNK6HGJZ'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=beauty_toppicks&banner=15WG15NQJNCDRTXVY382&f=ifr&linkID=F7KLFG5ICYPJEYAR'

    elif entity['category'] == 'camping-tents':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=outdoorrecreation&banner=0MBAF39GNAEMHM4QXY02&f=ifr&linkID=B4RTRDP67YYP2DW3'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=sports&banner=16JZ3TQKM4XZ0M64QS02&f=ifr&linkID=5PTITF3CVD4PPWTV'

    elif entity['category'] == 'photography-drones' or entity['category'] == 'recreational-drones':
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=htl2014&banner=0AHSBF73R0MFT3SP0VG2&f=ifr&linkID=7LAMFO6XGG7E4KRK'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=toysandgames&banner=1R959EDPJQ2ZH3TE4E82&f=ifr&linkID=4Z3CS2M7X5RTBBWI'

    else:
        ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=bestsellingproducts&banner=145H1HTA41NXJPV0C0R2&f=ifr&linkID=QOCJJRCQ3WWWIN3E'
        ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=amazonhomepage&f=ifr&linkID=QT7YHYM4DUJMGCCO'
        #ad1Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=tools&banner=008FGT7RDN7TXHHP7XR2&f=ifr&linkID=E24EH36JQ42QWJK3'
        #ad2Src = 'http://rcm-na.amazon-adsystem.com/e/cm?t=ibihuhacom-20&o=1&p=12&l=ur1&category=tools&banner=08CGRV8DW24QWP3NAGR2&f=ifr&linkID=FPMK6BTILBQLT52P'

    resp = make_response(render_template('product.html', title=title, entity=entity, sideContent=sideContent, related=related,
                           contentDescription=contentDescription, isEntityPage=isEntityPage, keywords=keywords,
                           ad1Src=ad1Src, ad2Src=ad2Src))
    resp.headers['Cache-Control'] = 'public, max-age=86400'
    return resp

@application.route('/reviews/<category>/<name>')
def getItem_deplicated(category, name):
    print 'IN DUPLICATE..'
    name = name.strip().replace('-', ' ').lower()
    entity = DataLayer.getItemDetailed(name)
    url = 'http://criticjam.com/reviews/' + category + '/' + entity['subCategory'] + '/' + name.replace(' ', '-')
    return redirect(url, code=301)

@application.route('/')
def home():
    pager = paginate()
    pagination = pager['pagination']
    searchFrom = pager['searchFrom']
    currentPage = pager['currentPage']
    morePages = True
    nonArticles = DataLayer.getNonArticlesTimeOrdered(searchFrom)
    lastSeenKey = nonArticles['lastSeen']
    if lastSeenKey is not None:
        lastSeenKey = serializeLastSeenKey(lastSeenKey)
    else:
        morePages = False
    entities = nonArticles['items']
    sideContent = DataLayer.getArticlesTimeOrdered()
    url = url_for('home',reviews='69105')
    title='CriticJam: Simple Reviews Summary: Making better choices quick.'
    contentDescription = 'CriticJam summarizes reviews into insights, wisdom of the crowd, for faster and better choices' \
                         ' among products and services. CriticJam leverages the wisdom of the crowd by aggregating, ' \
                         'analysing and summarizing thousands reviews and various opinions from the internet on sites such as ' \
                         'amazon.com, facebook.com, twitter.com, yelp.com, tripadvisor.com etc.., and it summarizes them ' \
                         'into features people talk about the most and how they feel, sentiment, about them.'

    if lastSeenKey is not None and currentPage > len(pagination):
        pagination.append(lastSeenKey)
    #print 'AFTER adding lastSeen pagination is: ', pagination
    isEntityPage = False
    keywords = 'criticjam, reviews summary, reviews, opinions, amazon reviews'
    resp = make_response(render_template('mainContent.html', title=title, contentDescription=contentDescription,
                                         entities=entities, sideContent=sideContent, pages=len(pagination),
                                         morePages=morePages, url=url, currentPage=currentPage, isEntityPage=isEntityPage, keywords=keywords))
    resp.set_cookie('pg', value=json.dumps(pagination))
    resp.headers['Cache-Control'] = 'public, max-age=86400'
    return resp

@application.route('/reviews/<category>')
def getCategory(category):
    print 'IN SEARCH'
    category = category.replace(' ', '-').lower()
    entities = []
    pager = paginate()
    pagination = pager['pagination']
    searchFrom = pager['searchFrom']
    currentPage = pager['currentPage']
    morePages = True
    lastSeenKey = None
    url = url_for('search', q=category)
    feat = None
    searchResults = DataLayer.searchCategory(category, searchFrom)
    if searchResults is not None:
        lastSeenKey = searchResults['lastSeenKey']
        entities = searchResults['items']
        searchPattern = searchResults['searchPattern']
    if lastSeenKey is not None:
        lastSeenKey = serializeLastSeenKey(lastSeenKey)
    else:
        morePages = False
    if lastSeenKey is not None and currentPage > len(pagination):
        pagination.append(lastSeenKey)
    sideContent = DataLayer.getArticlesTimeOrdered()
    title = category.capitalize() + ' Reviews Summary: ' + 'CriticJam'
    contentDescription = 'CriticJam: ' + category.capitalize() + ' reviews summaries based on thousands of reviews and various opinions.'
    keywords = category + ', criticjam, reviews summary, reviews, opinions, amazon reviews'
    resp = make_response(render_template('mainContent.html', title=title, contentDescription=contentDescription,
                                         entities=entities, sideContent=sideContent, pages=len(pagination),
                                         morePages=morePages, currentPage=currentPage, url=url, isSearch=False, query='', keywords=keywords))
    resp.set_cookie('pg', value=json.dumps(pagination))
    resp.set_cookie('searchpattern', value=searchPattern)
    if category is not None:
        resp.set_cookie('cat', value=category)
    if feat is not None:
        resp.set_cookie('feat', value=feat)
    resp.headers['Cache-Control'] = 'public, max-age=86400'
    return resp

def doSearch(queri=''):
    print 'IN DOSEARCH'
    entities = []
    searchResults = None
    pager = paginate()
    pagination = pager['pagination']
    searchFrom = pager['searchFrom']
    currentPage = pager['currentPage']
    morePages = True
    lastSeenKey = None
    feat = None
    category = request.cookies.get('cat')
    if queri == '':
        q = request.args.get('q')
        cat = request.args.get('cat')
        if cat is not None:
            if cat.lower() != 'all':
                cat = cat.strip().lower()
                q = cat + ' ' + q
    else:
        q = queri
    print 'Query is: ', q
    url = url_for('search', q=q)
    searchPattern = request.cookies.get('searchpattern')
    print 'CURRENT PAGE IS: ', currentPage
    searchResults = Search.query(search_term=q, lastSeenKey=searchFrom, currentPage=currentPage)
    if searchResults is not None:
        lastSeenKey = searchResults['lastSeenKey']
        entities = searchResults['items']
        category = searchResults['category']
        searchPattern = searchResults['searchPattern']
        if 'feat' in searchResults:
            feat = searchResults['feat']
    if lastSeenKey is not None:
        lastSeenKey = serializeLastSeenKey(lastSeenKey)
    else:
        morePages = False
    if lastSeenKey is not None and currentPage > len(pagination):
        pagination.append(lastSeenKey)
    sideContent = DataLayer.getArticlesTimeOrdered()
    title = 'Simple Reviews Summary: CriticJam, Making better choices quick.' if q is None else q + ': Simple Reviews Summary: CriticJam, Making better choices quick.'
    isSearch = False if q is None else True
    query = q if q is not None else ''
    contentDescription = q + ' Reviews Summary based on thousands of reviews and various opinions.'
    if category is not None:
        keywords = category + ', ' + q + ', criticjam, reviews summary, reviews, opinions, amazon reviews'
    else:
        keywords = q + ', criticjam, reviews summary, reviews, opinions, amazon reviews'

    if len(entities) > 0:
        pages = 1
        try:
            pages = int(request.args.get('pg'))
        except Exception as ex:
            print 'Error happened trying to parse pg query string: ', ex.message
        print 'PAGES ARE: ', pages, ' Morepages is: ', morePages, ' and CURRENT page is: ', currentPage
        resp = make_response(render_template('mainContent.html', title=title, contentDescription=contentDescription,
                                             entities=entities, sideContent=sideContent, pages=pages,
                                             morePages=morePages, url=url, currentPage=currentPage, isSearch=isSearch, query=query, keywords=keywords))
        resp.set_cookie('pg', value=json.dumps(pagination))
        resp.set_cookie('searchpattern', value=searchPattern)
        if category is not None:
            resp.set_cookie('cat', value=category)
        if feat is not None:
            resp.set_cookie('feat', value=feat)
        resp.headers['Cache-Control'] = 'public, max-age=86400'
        return resp
    else:
        if q is None:
            q = 'phone'
        if len(query) == 0:
            q = 'phone'
        resp = make_response(render_template('gSearch.html', title='Search', contentDescription='Search',
                                             pages=1, url='search', isSearch=True, query=q))
        resp.set_cookie('q', value=json.dumps(q))
        return resp

@application.route('/reviews')
def search():
    resp = doSearch()
    return resp

@application.route('/reviews/b/<brand>')
def brand(brand):
    brand = brand.replace(' ', '-').lower()
    resp = doSearch(queri=brand)
    return resp

@application.route('/reviews/bc/<brand>/<category>')
def brandCategory(brand, category):
    print 'IN BRAND CATEGORY............'
    category = category.replace(' ', '-').lower()
    brand = brand.replace(' ', '-').lower()
    resp = doSearch(queri=brand + ' ' + category)
    return resp

@application.route('/reviews/q/<query>')
def query(query):
    query = query.replace('+', ' ').lower()
    resp = doSearch(queri=query)
    return resp

@application.route('/search')
def googleSearch():
    query = request.args.get('q')
    search = request.args.get('search')
    if query is None:
        query = 'phone'
    print 'Query is: ', query
    print 'Search is: ', search
    resp = make_response(render_template('gSearch.html', title='Search', contentDescription='Search',
                                         pages=1, url='search', isSearch=True, query=query))
    resp.set_cookie('q', value=json.dumps(query))
    return resp


@application.errorhandler(404)
def page_not_found(e):
    return render_template('error.html'), 404

@application.errorhandler(500)
def page_not_found(e):
    return render_template('error.html'), 500

@application.route('/google8bb1b6cbee0e36a1.html')
def googleAuth():
    return render_template('google8bb1b6cbee0e36a1.html')


@application.route('/BingSiteAuth.xml')
def bingAuth():
    return render_template('BingSiteAuth.xml')

@application.route('/sitemap.txt')
def sitemap():
    return render_template('sitemap.txt')

@application.route('/yandex_52bb52c3cb252eee.html')
def yandexAuth():
    return render_template('yandex_52bb52c3cb252eee.html')


#@@@@@@@@@@@@@@@@@@@@@ NICHES @@@@@@@@@@@@@@@@@@@@@@@@@@@

@application.route('/reviews/2016/best-hoover-vacuum-cleaners')
def bestOfHoover():
    resp = make_response(render_template('niches/best-hoover-vacuum-cleaners.html'))
    resp.headers['Cache-Control'] = 'public, max-age=8640000'
    return resp

@application.route('/reviews/2016/best-dji-drones')
def bestOfDJI():
    resp = make_response(render_template('niches/best-dji-drones.html'))
    resp.headers['Cache-Control'] = 'public, max-age=8640000'
    return resp


#@@@@@@@@@@@@@@@@@@@@@ END NICHES @@@@@@@@@@@@@@@@@@@@@@@


#@@@@@@@@@@@@@@@@@@@@@ EXTENSIONS @@@@@@@@@@@@@@@@@@@@@@@@

def serializeLastSeenKey(lastSeenKey):
    keys = lastSeenKey.keys()
    for k in keys:
        if isinstance(lastSeenKey[k], Decimal):
            lastSeenKey[k] = float(lastSeenKey[k])
    return json.dumps(lastSeenKey)

def deSerializeLastSeenKey(lastSeenKey):
    lastSeenKey = json.loads(lastSeenKey)
    keys = lastSeenKey.keys()
    for k in keys:
        if isinstance(lastSeenKey[k], float):
            lastSeenKey[k] = Decimal(str(lastSeenKey[k]))
    return lastSeenKey

def paginate():
    page = request.args.get('pg')
    pagination = []
    searchFrom = None
    pgInt = 1
    if page is not None and page > '1':
        pg = request.cookies.get('pg')
        pgInt = int(page)
        if pg is not None:
            pagination = json.loads(pg)
            if (pgInt-1) <= len(pagination) and (pgInt-2) >= 0:
                searchFrom = deSerializeLastSeenKey(pagination[pgInt-2])
                print 'searchFrom in PAGINATE is: ', searchFrom

    return {'pagination': pagination, 'searchFrom': searchFrom, 'currentPage': pgInt}

if __name__ == '__main__':
    application.debug = True
    application.run()